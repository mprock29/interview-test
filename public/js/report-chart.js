
function setDate(month, year) {
    console.log('month', month)
    console.log('year', year)

    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    $('.month-display').text(months[month - 1] + ' ' + year);
    $('#month').val(new Date(year, month - 1, 01).toLocaleDateString('en-CA')) // 2020-08-19 (year-month-day) notice the different locale
    getChartData();
}
localStorage.clear('setmonth');
localStorage.clear('setyear');
// previous button function
$('#previous_line_chart').click(function () {
    $('.next-btn').removeClass('disabled');
    var oldmonth = localStorage.getItem("setmonth");
    var oldyear = localStorage.getItem("setyear");
    if (oldmonth != null && oldyear != null) {
        getmonth = oldmonth;
        getyear = oldyear;
        if (getmonth == 1) {
            getyear = parseInt(getyear) - 1;
            localStorage.setItem("setyear", getyear);
            var getmonth = 13;
        } else {
            localStorage.setItem("setyear", parseInt(getyear));
        }
        localStorage.setItem("setmonth", parseInt(getmonth) - 1);
    } else {
        var d = new Date();
        d.setMonth(d.getMonth() + 1);
        var getmonth = d.getMonth();
        var getyear = d.getFullYear();
        if (getmonth == 1) {
            getyear = parseInt(getyear) - 1;
            localStorage.setItem("setyear", getyear);
            var getmonth = 13;
        } else {
            localStorage.setItem("setyear", parseInt(getyear));
        }
        localStorage.setItem("setmonth", parseInt(getmonth) - 1);
    }
    console.log(parseInt(getmonth) - 1)
    console.log(getyear)
    setDate(parseInt(getmonth) - 1, getyear)
});


// next button function
$('#next_line_chart').click(function () {
    if (!$(".next-btn").hasClass("disabled")) {
        $('#line-chart').html('');
        var oldmonth = localStorage.getItem("setmonth");
        var oldyear = localStorage.getItem("setyear");
        if (oldmonth != null && oldyear != null) {
            getmonth = oldmonth;
            getyear = oldyear;
            if (getmonth == 12) {
                getyear = parseInt(getyear) + 1;
                localStorage.setItem("setyear", getyear);
                var getmonth = 0;
            }
            localStorage.setItem("setyear", parseInt(getyear));
            localStorage.setItem("setmonth", parseInt(getmonth) + 1);
        } else {
            var d = new Date();
            var getyear = d.getFullYear();
            d.setMonth(d.getMonth() + 1);
            var getmonth = d.getMonth();
            if (getmonth == 12) {
                getyear = parseInt(getyear) + 1;
                localStorage.setItem("setyear", getyear);
                var getmonth = 0;
            }
            localStorage.setItem("setyear", parseInt(getyear))
            localStorage.setItem("setmonth", parseInt(getmonth) + 1);
        }
        var current_month = new Date();
        if (oldmonth == current_month.getMonth() && oldyear == current_month.getFullYear()) {
            $('.next-btn').addClass('disabled');
        }
        console.log(parseInt(getmonth) + 1)
        console.log(getyear)
        setDate(parseInt(getmonth) + 1, getyear)
    }
});


// Define data set for all charts
let normalCount = longCount = multipleCount = averageCount = sum_total_count = [];
let labels = [];
myData = {
    labels: labels,
    datasets: [
        // {
        //     label: "Normal Count",
        //     fill: false,
        //     backgroundColor: 'rgb(190, 99, 255, 0.50)',
        //     borderColor: 'rgb(190, 99, 255)',
        //     data: normalCount,
        // },
        // {
        //     label: "Long Count",
        //     fill: false,
        //     backgroundColor: 'rgba(255, 99, 132, 0.50)',
        //     borderColor: 'rgba(255, 99, 132)',
        //     data: longCount,
        // },
        // {
        //     label: "Multiple Count",
        //     fill: false,
        //     backgroundColor: 'rgb(75, 192, 192, 0.50)',
        //     borderColor: 'rgb(75, 192, 192)',
        //     data: multipleCount,
        // },
        {
            label: "Average Count",
            fill: false,
            backgroundColor: 'rgb(23, 162, 184, 0.75)',
            borderColor: 'rgb(23, 162, 184)',
            borderDash: [5, 5],
            data: averageCount,
        },
        {
            label: "Total Count",
            fill: false,
            backgroundColor: 'rgb(5, 190, 1, 0.75)',
            borderColor: 'rgb(5, 190, 1)',
            // borderDash: [5, 5],
            data: sum_total_count,
        }
    ],
    options: {
        maintainAspectRatio: false,
        responsive: false,
        scales: {
          y: {
            stacked: true,
            grid: {
              display: true,
              color: "rgba(255,99,132,0.2)"
            }
          },
          x: {
            grid: {
              display: false
            }
          }
        }
    }
};
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: myData
});

function getChartData() {
    $(".myChart-loading").show();
    myChart.destroy();
    var request = [];
    request['group_id'] = $('.select2-group').val();
    request['vehicle_id'] = $('.select2-vehicle').val();
    request['driver_id'] = $('.select2-driver').val();
    request['month'] = $('#month').val();
    request['_token'] = $("input[name=_token]").val();

    $.ajax({
        type: 'POST',
        data: {
            _token: $("input[name=_token]").val(),
            group_id: $('.select2-group').val(),
            vehicle_id: $('.select2-vehicle').val(),
            driver_id: $('.select2-driver').val(),
            month: $('#month').val(),
        },
        url: '/horn-summary-chart',
        success: function (response) {
            console.log('response', response)
            $(".myChart-loading").hide();
            // myData.datasets[0].data = response.total.sum_normal_horn_total_count
            // myData.datasets[1].data = response.total.sum_long_horn_total_count
            // myData.datasets[2].data = response.total.sum_multiple_total_horn_count
            myData.datasets[0].data = response.total.average_count
            myData.datasets[1].data = response.total.sum_total_count
            myData.labels = response.total.labels;
            myChart = new Chart(ctx, {
                type: document.getElementById("chartType").value,
                data: myData,
            });
        }
    });
}

getChartData();
$('#chart-form').on('keyup change paste', 'input, select', function (event) {
    event.preventDefault();
    getChartData();
});

