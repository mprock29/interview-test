$(document).ready(function () {
    refreshPage();
    $("#refresh-dashboard").on('click', function() {
        refreshPage();
    })
});

function refreshPage() {
    if ($('#user_count').length) {
        getUserCount();
    }

    if ($('#service_provider_count').length) {
        getServiceProviderCount();
    }

    if ($('#technician_count').length) {
        getTechnicianCount();
    }

    if ($('#normal_user_count').length) {
        getNormalUserCount();
    }

    if ($('#pending_order_count').length) {
        getPendingOrderCount();
    }

    if ($('#complete_order_count').length) {
        getCompletedOrderCount();
    }

    if ($('#dashboard_order_list').length) {
        getOrderList();
    }
    
}


function getCompletedOrderCount() {
    $.get("/dashboard/complete_order_count", function (data) {
        $("#complete_order_count").html(data.total);
    });
}

function getPendingOrderCount() {
    $.get("/dashboard/pending_order_count", function (data) {
        $("#pending_order_count").html(data.total);
    });
}

function getNormalUserCount() {
    $(".normal_user_count").show();
    $.get("/dashboard/normal_user_count", function (data) {
        $("#normal_user_count").html(data.total);
        $(".normal_user_count").hide();
    });
}

function getTechnicianCount() {
    $(".technician_count").show();
    $.get("/dashboard/technician_count", function (data) {
        $("#technician_count").html(data.total);
        $(".technician_count").hide();
    });
}

function getUserCount() {
    $(".user_count").show();
    $.get("/dashboard/user_count", function (data) {
        $("#user_count").html(data.total);
        $(".user_count").hide();
    });
}

function getServiceProviderCount() {
    $(".service_provider_count").show();
    $.get("/dashboard/service_provider_count", function (data) {
        $("#service_provider_count").html(data.total);
        $(".service_provider_count").hide();
    });
}

function getOrderList() {
    $(".dashboard_order_list").show();
    $.get("/dashboardorderlist", function (data) {
        $("#dashboard_order_list").html(data);
        $(".dashboard_order_list").hide();
        var order_count = $('#order_count_dashboard').text();
        $(".total_count_today_orders").html(order_count);

    });
}


