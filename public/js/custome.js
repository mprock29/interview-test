$(function () {
    //Modal Confirm
    // $('#modal-delete-confirm').on('shown.bs.modal', function (e) {
    //     var href = $(e.relatedTarget).attr('data-url');
    //     $(e.currentTarget).find('a').attr('href', href);
    // });

    // focus on select2 search
    $(document).on('select2:open', () => {
       document.querySelector('.select2-search__field').focus();
    });
});


// $('.delete_confirm').on('click', function (e) {
    $(document).on("click",".delete_confirm", function(e){
        var href = $(e.target).parents().closest('a').attr('data-url');
        Swal.fire({
            title: 'Are you sure?',
            text: 'Do you really want to delete this record',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes Delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                document.location.href=href
            }
        });
});

$(document).ready(function () {
    setTimeout(function(){
        $("div.alert").fadeOut();
    }, 4000 );

    $(':input').on("keyup change", function () {
        $(this).parent('.has-error').find("span.help-block").hide();
        $(this).parent('.has-error').removeClass("has-error");
    })

    $('#phone,#phonecode').keypress(function (event) {
        if ((event.which != 44 || $(this).val().indexOf(',') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('#date_range').daterangepicker({
        autoUpdateInput: false,
        locale: {
            format: 'DD/MM/YYYY',
            cancelLabel: 'Clear'
        }
    });

    $('#date_range').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('#date_range').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    // Check or Uncheck All checkboxes
    // $(".select-all").change(function () {
    $(document).on("change",".select-all", function(e){
        var checked = $(this).is(':checked');
        if (checked) {
            $(".checkbox").each(function () {
                $(this).prop("checked", true);
            });
        } else {
            $(".checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });

    // Changing state of CheckAll checkbox
    // $(".checkbox").click(function () {
    $(document).on("click",".checkbox", function(e){
        if ($(".checkbox").length == $(".checkbox:checked").length) {
            $("#checkall").prop("checked", true);
        } else {
            $("#checkall").prop("checked", false);
        }
    });

    $(document).on('click', '#btn_active, #btn_inactive, #btn_delete', function (event) {
        if ($('input:checkbox').filter(':checked').length < 1) {
            Swal.fire({
                title: 'Error!',
                text: 'Please select atleast one checkbox',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
            return false;
        }
        var status = $(this).val();
        var perform = '';
        if (status == '0') {
            perform = 'Inactive';
        }
        else if (status == '1') {
            perform = 'Active';
        }
        else {
            perform = 'Delete';
        }

        var title_message = 'Are you sure?';
        var no_revert = 'You want to '+perform+' selected record!';
        var confirm_delete = 'Yes, '+perform+' it!';

        Swal.fire({
            title: 'Are you sure?',
            text: no_revert,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: confirm_delete
        }).then((result) => {
            if (result.isConfirmed) {

                event.preventDefault();
                var url = $(this).attr('data-url');
                console.log("url: " + url);

                const roleIds = [];
                $(".checkbox").each(function () {
                    if ($(this).prop('checked')) {
                        roleIds.push($(this).val());
                        console.log("RoleIDs: " + roleIds);
                    }
                });


                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        _token: $("input[name=_token]").val(),
                        id: roleIds,
                        status: status,
                    },
                    success: function (response) {
                        // $("#roleIDs").empty().html(response);

                        if (status == '0') {
                            toastr.success('Inactivated Successfully.')
                        }
                        else if (status == '1') {
                            toastr.success('Activated Successfully.')
                        }
                        else {
                            toastr.success('Deleted Successfully.')
                        }

                        // Check or Uncheck All checkboxes
                        $(".select-all").change(function () {
                            var checked = $(this).is(':checked');
                            if (checked) {
                                $(".checkbox").each(function () {
                                    $(this).prop("checked", true);
                                });
                            } else {
                                $(".checkbox").each(function () {
                                    $(this).prop("checked", false);
                                });
                            }
                        });

                        // Changing state of CheckAll checkbox
                        $(".checkbox").click(function () {
                            if ($(".checkbox").length == $(".checkbox:checked").length) {
                                $("#checkall").prop("checked", true);
                            } else {
                                $("#checkall").prop("checked", false);
                            }
                        });
                        document.location.reload()
                    }
                });
            }
        });
    });

    if ($('.has-error').length > 0) {
        $('html, body').animate({
            scrollTop: $(".has-error").offset().top
        }, 2000);
    }

    // $(".reset").click(function () {
    $(document).on("click",".reset", function(){
        $(this).closest('form').find("input[type=text], textarea, select").val("");
    });

    $(".status, .parent_menu, .child_menu, .brand_id, .role_id, .select2-preferred_lang, .city, .is_25_point_inspection_free, .free_pickup_drop, .service_category_id, .service_type ").select2({
        tags: false,
        tokenSeparators: [',', ' ']
    });

    if ($('#ajax-sortable-container').length >= 1) {
        $(document).on('click', '.ajax-sortable', function (event) {
            event.preventDefault();
            let sortable_link = $(this).attr('href');
            let sort_table = this;
            history.replaceState({}, "", sortable_link)
            removeParam('_token')
            $.ajax({
                url: sortable_link,
                type: 'POST',
                data: {
                    _token: $("input[name=_token]").val(),
                },
                async: false,
                success: function (response) {
                    $(sort_table).parents("#ajax-sortable-container").empty().html(response);
                    numberFormatClass();
                    // $("#ajax-sortable-container").empty().html(response);
                }
            });
        });
    }

    function removeParam(parameter)
    {
        var url=document.location.href;
        var urlparts= url.split('?');

        if (urlparts.length>=2)
        {
            var urlBase=urlparts.shift();
            var queryString=urlparts.join("?");

            var prefix = encodeURIComponent(parameter)+'=';
            var pars = queryString.split(/[&;]/g);
            for (var i= pars.length; i-->0;)
                if (pars[i].lastIndexOf(prefix, 0)!==-1)
                    pars.splice(i, 1);
            url = urlBase+'?'+pars.join('&');
            window.history.pushState('',document.title,url); // added this line to push the new url directly to url bar .
        }
        return url;
    }


    $(document).on('change', '.get_child_menu', function (e) {
        var parent_id = $(this).val();
        console.log("parent_id: " + parent_id);

        $.ajax({
            url: '/get_child_menu',
            type: 'POST',
            data: {
                _token: $("input[name=_token]").val(),
                parent_id: parent_id,
            },
            async: false,
            beforeSend: function beforeSend() { },
            success: function success(response) {
                console.log(response.message);
                if (response.success) {
                    $("#child_menu_id").html(response.message);
                    $("#child_menu_id").select2();
                }
            }
        });
    });
});

//common number format functions
function numberFormatClass() {
    $('.numberFormatChange').text(function () {
        return parseFloat($(this).text()).toLocaleString('de-DE')
    })

    $('.numberFormatChangeInput').val(function () {
        if ($(this).val() != '' && $(this).val() != null && !isNaN(parseFloat($(this).val()))) {
            return parseFloat($(this).val()).toLocaleString('de-DE')
        } else {
            return $(this).val()
        }
    })
}

function readURLMultiple(input, count) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (input.files[0]['type'].split('/')[0] === 'image') {
                file_url = URL.createObjectURL(input.files[0]);
                $("#imgPreview").parent('div').parent('div').find('span.help-block').hide();
                $("#imgPreview").removeClass('d-none');
                $("#imgPreview").attr('src', file_url);
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function showMediaImage(input, count) {
    if (input.files && input.files[0]) {
        
        var image_html = "";
        [...input.files].forEach(child => {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (child['type'].split('/')[0] === 'image') {
                    file_url = URL.createObjectURL(child);
                    image_html += '<div class="col-md-2"> <div id="upload_0_image"> <img class="imgPreview img-thumbnail" src="'+file_url+'" id="imgPreview" alt="Image" style="margin-bottom: 15px; height: 100px; width: 100px;"/> </div></div>'
                    $('.viewimages').html(image_html);
                }
            };
            reader.readAsDataURL(child);
          });
        
    }
}


function getCookie(cName) {
    const name = cName + "=";
    const cDecoded = decodeURIComponent(document.cookie); //to be careful
    const cArr = cDecoded.split('; ');
    let res;
    cArr.forEach(val => {
      if (val.indexOf(name) === 0) res = val.substring(name.length);
    })
    return res
  }

$('[data-widget=pushmenu]').on('click',function(){
    var cls =  $('body').hasClass('sidebar-collapse');
    var collapse = getCookie('collapse');
    if(collapse){
        document.cookie = "collapse="+!JSON.parse(collapse)+";path=/;";
    } else {
        document.cookie = "collapse="+!JSON.parse(cls)+";path=/;";
    }
});

// catch global exception
$( document ).ajaxError(function() {
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
  });
