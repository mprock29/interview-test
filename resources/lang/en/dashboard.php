<?php
return array(
    "lbl_users" => "No. of Users",
    "lbl_app_users" => "No. of App Users",
    "lbl_service_provider" => "No. of Service Provider",
    "lbl_technician" => "No. of Technicians",
    "lbl_pending_orders" => "Pending Orders",
    "lbl_complete_orders" => "Completed Orders",
    "lbl_vehicle" => "No. of Vehicle",
    "lbl_driver" => "No. of Drivers",
    "lbl_email" => "Email Logs",
    "lbl_sms" => "SMS Logs",
    "lbl_device" => "Devices Logs",
    "lbl_devices" => "No. of Device",
    "lbl_horn_duration_report" => "Horn Duration Report",
    "lbl_horn_summery_report" => "Horn Summary Report",  
);
