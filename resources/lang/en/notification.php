<?php

return array(   
    'lbl_notification' => 'Notifications',     
    'lbl_notification_list' => 'Notification List',
    'lbl_notification_name_select' => 'Select Notification Name',
    'lbl_user_name' => 'User Name',
    'lbl_message' => 'Message',
    'lbl_is_sent' => 'Is sent',
);
?>
