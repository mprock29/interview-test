<?php


return array(
    /*Labels */
    'lbl_current_password' => 'Current Password',
    'lbl_new_password' => 'New Password',
    'lbl_new_confirm_password' => 'Confirm Password',
    'lbl_change_password' => 'Change Password',
    'lbl_dashboard' => 'Dashboard',
    'lbl_change_password' => 'Change Password',
    'lbl_change_profile' => 'Change Password',
);
