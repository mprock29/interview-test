<?php

return array(
    "lbl_menus" => "Menus",
    "lbl_menus_create" => "Create Menu",
    "lbl_menus_edit" => "Edit Menu",
    "lbl_menus_delete" => "Delete Menu",
    "lbl_menus_manage" => "Manage Menu",
    "lbl_menu_title" => "Menu Title",
    "lbl_page_url" => "Page URL",
    "lbl_parent_menu" => "Parent Menu",
    "lbl_child_menu" => "Child Menu",
    "lbl_menu_icon" => "Menu Icon",
    "lbl_show_in_menu" => "Is Show Menu",
    "lbl_show_menu" => "Show Menu",
    "lbl_select_parent_menu" => "Select Parent Menu",
    "lbl_select_child_menu" => "Select Child Menu",
    "lbl_select_menu_icon" => "Select Menu Icon",
    'lbl_menus_added' => 'Menu added successfully.',
    'lbl_menus_updated' => 'Menu updated successfully.',
    'lbl_menus_deleted' => 'Menu deleted successfully.',
    'lbl_menus_sortorder' => 'Sort Order Menu',
    'lbl_sort_order' => 'Sort Order'
);
