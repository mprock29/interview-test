<?php

return array(
    /*Labels */
    'lbl_full_name' => 'Full Name',
    'lbl_user_name' => 'User Name',
    'lbl_phone' => 'Phone Number',
    'lbl_email' => 'Email-Address',
    'lbl_edit_user_details' => 'Edit User Profile',
    'lbl_user' => 'User',
    'lbl_update_profile' => 'User Profile',
);
