<?php

return array(
    "lbl_roles" => "Roles",
    "lbl_roles_list" => "Roles List",
    "lbl_roles_create" => "Create Role",
    "lbl_roles_edit" => "Edit Role",
    "lbl_roles_delete" => "Delete Role",
    "lbl_roles_name" => "Role Name",
    'lbl_groups_name' => 'Group Name',
    'lbl_roles_name_select' => 'Select Role Name',
    "lbl_roles_set_permissions" => "Set Permissions",
    'lbl_roles_permissions' => "Manage Permissions ",
    'lbl_roles_added' => 'Roles successfully added.',
    'lbl_roles_updated' => 'Role updated successfully.',
    'lbl_roles_deleted' => 'Role deleted successfully.',
    'lbl_roles_permissions_edit' => 'Permissions Updated Successfully.',
    'lbl_total_users' => 'Total Users'
);
