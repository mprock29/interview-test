@extends('layouts.app')
@section('title', trans('update_profile.lbl_update_profile'))
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="{{ route('home') }}">{{ trans('common.lbl_dashboard') }}</a>
                            </li>
                            <li class="breadcrumb-item active">
                                {{ trans('update_profile.lbl_update_profile') }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ trans('update_profile.lbl_update_profile') }}</h4>
                            </div>
                            {!! Form::open(['route' => ['user.edit'], 'method' => 'POST', 'id' => 'user_update', 'method' => 'patch', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) !!}
                            {{-- {{ Form::model(['route' => ['home'], 'method' => 'patch', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }} --}}
                            <div class="card-body">
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                                            {{ Form::label('full_name', trans('update_profile.lbl_full_name'), ['class' => 'control-label']) }}
                                            <span class="required">*</span>
                                            {{ Form::input('text', 'full_name', \Auth::user()->full_name, ['id' => 'full_name', 'class' => 'form-control', 'placeholder' => trans('update_profile.lbl_full_name')]) }}
                                            @if ($errors->has('full_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('full_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                            {{ Form::label('username', trans('update_profile.lbl_user_name'), ['class' => 'control-label']) }}
                                            <span class="required">*</span>
                                            {{ Form::input('text', 'username', \Auth::user()->username, ['id' => 'username', 'class' => 'form-control', 'placeholder' => trans('update_profile.lbl_user_name'), 'disabled']) }}
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            {{ Form::label('email', trans('update_profile.lbl_email'), ['class' => 'control-label']) }}
                                            <span class="required">*</span>
                                            {{ Form::input('text', 'email', \Auth::user()->email, ['id' => 'email', 'class' => 'form-control', 'placeholder' => trans('update_profile.lbl_email'), 'disabled']) }}
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                            {{ Form::label('phone', trans('update_profile.lbl_phone'), ['class' => 'control-label']) }}
                                            <span class="required">*</span>
                                            {{ Form::input('text', 'phone', \Auth::user()->phone, ['id' => 'phone', 'class' => 'form-control', 'placeholder' => trans('update_profile.lbl_phone')]) }}
                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>                                                                     
                                   
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('profile_pic') ? ' has-error' : '' }}">
                                            {{ Form::label('profile_pic', trans('users.lbl_profile_image'), ['class' => 'control-label']) }}
                                            <div class="custom-file">
                                                <div id="upload_0_image">
                                                    <input type="file" name="profile_pic" id="profile_pic"
                                                        class="custom-file-input" onchange="readURLMultiple(this, 0)">
                                                    <label class="custom-file-label" for="profile_pic">Choose
                                                        file...</label>
                                                </div>
                                            </div>
                                            @if ($errors->has('profile_pic'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('profile_pic') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        @if (isset(auth()->user()->profile_pic) && !empty(auth()->user()->profile_pic))
                                            <div id="upload_0_image">
                                                <img class="imgPreview" id="imgPreview"
                                                    src="{{ auth()->user()->profile_pic }}"
                                                    alt="your image"
                                                    style="margin-bottom: 15px; height: 100px; width: 100px;">
                                            </div>
                                        @else
                                            <div id="upload_0_image">
                                                <img class="d-none imgPreview" id="imgPreview" alt="Image"
                                                    style="margin-bottom: 15px; height: 100px; width: 100px;" />
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('view_mode') ? ' has-error' : '' }}">
                                            {{ Form::label('view_mode', trans('users.lbl_view_mode_name'), ['class' => 'control-label']) }}
                                            {{ Form::select('view_mode', $viewMode, \Auth::user()->view_mode, ['class' => 'form-control select2-marital_status', 'placeholder' => trans('users.lbl_select_view_mode_status')]) }}
                                            @if ($errors->has('view_mode'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('view_mode') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ Form::submit(trans('common.btn_save'), ['class' => 'btn btn-primary btn-lg']) }}
                                    {{ Html::link(route('home'), trans('common.btn_cancel'), ['class' => 'btn btn-secondary btn-lg']) }}
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="/js/inputmask.min.js"></script>
    <script type="text/javascript">
        $('#phone').inputmask("mask", {
            "mask": "9{10}",
            removeMaskOnSubmit: false,
            autoclear: false,
            autoUnmask: true,
            insertMode: true
        });
        $(document).ready(function() {
            //Date picker
            $('#date_of_birth').datetimepicker({
                format: @json(trans('common.date_formate')),
                // viewMode: 'years',
                autoclose: true,
                allowInputToggle: true,
                useCurrent: false,
                maxDate: new Date(),
            });



            $('#anniversary_date').datetimepicker({
                format: @json(trans('common.date_formate')),
                // viewMode: 'years',
                autoclose: true,
                allowInputToggle: true,
                useCurrent: false,
                maxDate: new Date(),
            });



            $(document).ready(function() {
                $(".select2-marital_status").select2();
                $('.anniversary_date').hide();
                if($('#full_name').val()=="")
                {
                    $('input[name="date_of_birth"]').val('');
                }
                var status = $('.select2-marital_status').val();
                // console.log(status);
                if (status == '0') {
                    $('.anniversary_date').show();
                }else{
                    $('input[name="anniversary_date"]').val('');
                    $('.anniversary_date').hide();
                }
            });

            $(".select2-marital_status").on('change', function() {
                var $status = $('.select2-marital_status');
                var statusId = $status.val();
                // console.log(statusId);

                if (statusId == '0') {
                    $('.anniversary_date').show();
                } else {
                    $('input[name="anniversary_date"]').val('');
                    $('.anniversary_date').hide();
                }
            });


        });
    </script>

@endsection
