@extends('layouts.login')

@section('content')
    <div class="container">
                <div class="login-box">
                    <div class="card card-outline card-default">
                        <div class="card-header text-center">
                        <img src="{{ asset('img/logo.png') }}" class="img-circle elevation-2" alt="User Image"
                            width="100">
                    </div>
                    <div class="login-box-msg">{{ __('Reset Password') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-group row">

                                {{-- <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label> --}}
                                <div class="input-group col-md-12">

                                    <input id="email" type="email" placeholder="E-mail Address"
                                        class="form-control @error('email') is-invalid @enderror col-md-16" name="email"
                                        value="{{ old('email') }}" autocomplete="email" autofocus>

                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <span class="fas fa-envelope"></span>
                                        </div>
                                    </div>
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>


                            <div class="form-group row">
                                <div class="col-md-12 offset-1">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                                <a class="btn btn-primary btn-close" href="{{ route('login') }}"> Back To Login</a>
                            </div>
                    </div>
                </div>
                </form>
            </div>
        </div>


    </div>
</div>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
@endsection
