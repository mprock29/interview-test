@extends('layouts.login')

@section('content')
<div class="container">
            <div class="login-box">
                <div class="card  card-default">
                    <div class="card-header text-center">
                        <img src="{{ asset('img/logo.png') }}" class="img-circle elevation-2" alt="User Image" width="100">
                    </div>
             <div class="card-body">
                <div class="login-box-msg">{{ __('Reset  Your Password') }}</div>

                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                        <input type="hidden" name="email" value="{{ $email }}">

                        {{--  <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>  --}}

                        <div class="form-group row">
                            <div class="input-group col-md-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"  placeholder="New Password" name="password"  autocomplete="new-password">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="input-group col-md-12">
                                <input id="password-confirm" type="password" class="form-control col-md-16" placeholder="New Confirm Password" name="password_confirmation"  autocomplete="new-password">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 offset-1">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                                <a class="btn btn-primary btn-close" href="{{ route('login') }}"> Back To Login</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
</div>

@endsection
