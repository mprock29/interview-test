@extends('layouts.app')
@section('title', trans('change_password.lbl_change_profile'))
@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin/home">{{ trans('change_password.lbl_dashboard') }}</a></li>
                            <li class="breadcrumb-item active">{{ trans('change_password.lbl_change_password') }}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{ trans('change_password.lbl_change_password') }}</h4>
                            </div>

                            {{ Form::model(['route' => ['change.password'], 'method' => 'post', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) }}
                                <div class="card-body">
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                                                {{ Form::label('current_password', trans('change_password.lbl_current_password'), ['class' => 'control-label']) }}
                                                <span class="required">*</span>
                                                {{ Form::password('current_password',['id' => 'current_password',  'class' => 'form-control', 'placeholder' => trans('change_password.lbl_current_password')]) }}
                                                @if ($errors->has('current_password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('current_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                                                {{ Form::label('current_password', trans('change_password.lbl_new_password'), ['class' => 'control-label']) }}
                                                <span class="required">*</span>
                                                {{ Form::password('new_password', ['id' => 'new_password', 'class' => 'form-control', 'placeholder' => trans('change_password.lbl_new_password')]) }}
                                                @if ($errors->has('new_password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('new_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <div
                                                class="form-group{{ $errors->has('new_confirm_password') ? ' has-error' : '' }}">
                                                {{ Form::label('new_confirm_password', trans('change_password.lbl_new_confirm_password'), ['class' => 'control-label']) }}
                                                <span class="required">*</span>
                                                {{ Form::password('new_confirm_password', ['id' => 'new_confirm_password', 'class' => 'form-control', 'placeholder' => trans('change_password.lbl_new_confirm_password')]) }}
                                                @if ($errors->has('new_confirm_password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('new_confirm_password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    {{ Form::submit(trans('common.btn_save'), ['class' => 'btn btn-primary btn-lg']) }}
                                    {{ Html::link(route('home'), trans('common.btn_cancel'), ['class' => 'btn btn-secondary btn-lg']) }}
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
        </section>
    </div>
@endsection
