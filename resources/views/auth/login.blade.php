@extends('layouts.login')
@php
    if(!session()->has('url.intended'))
    {
        // echo url()->previous();
        session(['url.intended' => url()->previous()]);
    } else {
        // echo url()->previous();
        // session(['url.intended' => url()->previous()]);
    }
@endphp

@section('content')
<div class="container">
    <div class="login-box">
        @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h5><i class="icon fas fa-ban"></i>{{ trans('common.lbl_error_alert')}}</h5>
            {{ Session::get('error') }}
        </div>
        @endif
            <div class="card card-outline card-primary">
                <div class="card-header text-center">
                    <img src="{{ asset('img/logo.png') }}" class="" alt="User Image" width="100">
                    {{-- <img src="{{ asset('img/logo.png') }}" class="img-circle elevation-2" alt="User Image" width="100"> --}}
                </div>

                <div class="card-body">
                    <p class="login-box-msg">Log in to start your session</p>
                    {{ Form::open(['method' => 'POST','route' => ['login'] ])  }}
                        <div class="input-group mb-3 {{ $errors->has('email') ? ' has-error' : '' }} ">
                            {{ Form::text('email',(!empty(Cookie::get('email'))) ? Cookie::get('email') : old('email'), ['id'=>'email','class'=>'form-control','placeholder'=> 'E-Mail Address','autocomplete'=>'email']) }}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-envelope"></span>
                                    </div>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="input-group mb-3 {{ $errors->has('password') ? ' has-error' : '' }}">
                            {{ Form::input('password','password', (!empty(Cookie::get('password'))) ? Cookie::get('password') : old('password') ,['class'=>'form-control','id'=>'password', 'placeholder'=>'Password','autocomplete'=>'current-password']) }}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                            <div class="row">
                                <div class="col-8">
                                    <div class="icheck-primary">
                                        {{ Form::checkbox('remember_token',null,(!empty(Cookie::get('email')) &&!empty(Cookie::get('password'))) ? true : false,['id'=>'remember_token','name'=>'remember_token'])}}
                                        {{ Form::label('remember_token','Remember Me', ['class' => 'control-label']) }}
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-4">
                                    <button type="submit" class="btn btn-primary btn-block">Log In</button>
                                </div>
                                <!-- /.col -->
                            </div>

                    {{ Form::close() }}

                    <p class="mb-1">
                        @if (Route::has('password.request'))
                          <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                          </a>
                        @endif
                    </p>

                </div>
            <!-- /.card-body -->
            </div>
        <!-- /.card -->
    </div>
    <!-- /.login-box -->
</div>
@endsection
@section('script')
    <script>
        $(':input').on("keyup change", function () {
            $(this).parent('.has-error').find("span.help-block").hide();
            $(this).parent('.has-error').removeClass("has-error");
        });
</script>
@endsection
