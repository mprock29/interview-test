<div class="modal fade" id="modal-delete-confirm">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ trans('common.lbl_warning') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete this record?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans('common.btn_close') }}</button>
                <a href="javascript:;" class="btn btn-primary perform-action">{{ trans('common.lbl_delete')}} </a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-select-checkbox">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ trans('common.lbl_warning') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Please select atleast one checkbox.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">{{ trans('common.btn_ok') }}</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete-checkbox-confirm">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ trans('common.lbl_warning') }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete the selected record?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans('common.btn_close') }}</button>
                <a href="javascript:;" class="btn btn-primary perform-action">{{ trans('common.lbl_delete')}} </a>
            </div>
        </div>
    </div>
</div>