<div class="container-fluid">
    <div class="page-count">
        Showing {{ ($pagination->currentPage() - 1) * $pagination->perPage() + 1 }} -
        {{ ($pagination->currentPage() - 1) * $pagination->perPage() + $pagination->count() }} out of
        {{ $pagination->total() }} entries
    </div>



    <div class="pagination pagi m-0 pagination-sm ">
        {{ $pagination->appends($next_query)->onEachSide(1)->links() }}
    </div>
</div>