<nav class="main-header navbar navbar-expand  {{(auth()->user()->view_mode == '0')?'navbar-black navbar-dark':'navbar-white navbar-light'}}">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" id="toggleButton" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                   
                    {{ (Auth::user()->first_name != '' && Auth::user()->last_name != '') ? (Auth::user()->first_name .' '. Auth::user()->last_name) : Auth::user()->username }}<span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right logout_menu" aria-labelledby="navbarDropdown">
                    
                    <a class="dropdown-item" href="{{ route('change.password') }}">
                        {{ trans('common.lbl_change_password') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                        {{ trans('common.lbl_logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
</nav>
