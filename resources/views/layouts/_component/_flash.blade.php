@if(Session::has('success') || Session::has('error') || Session::has('restricted'))
<div class="container-fluid">
    <div class="justify-content-center flash-msg ">
        <div class=" card-default">
            <div class="row flash_message_div">
                <div class="col-md-12">
                    @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-ban"></i>{{ trans('common.lbl_error_alert')}}</h5>
                        {{ Session::get('error') }}
                    </div>
                    @endif
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-check"></i>{{ trans('common.lbl_success_alert')}}</h5>
                        {{ Session::get('success') }}
                    </div>
                    @endif
                    @if(Session::has('restricted'))
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-user-slash"></i>{{ trans('common.lbl_restricted_alert')}}</h5>
                        {{ Session::get('restricted') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endif