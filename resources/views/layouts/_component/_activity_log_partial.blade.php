@if ($activity_data->isNotEmpty())

    @php
        $Common = new App\Utility\Common();
    @endphp
    <div class="card-body table-responsive">
        <table class="table table-bordered table-hover dtr-inline ">
            <thead>
                <tr style="border-top:1px solid #dee2e6;">
                    {{-- <th scope="col">#</th> --}}
                    {{-- <th scope="col">{{ trans('activity.lbl_activity_log_name') }}</th> --}}
                    <th scope="col">{{ trans('activity.lbl_activity_description') }}</th>
                    <th scope="col">{{ trans('activity.lbl_activity_changed_by') }}</th>
                    <th scope="col">{{ trans('activity.lbl_activity_changes') }}</th>
                    <th scope="col">{{ trans('activity.lbl_activity_at') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($activity_data as $key => $value)
                    <tr>
                        {{-- <td> {{ $value->id }} </td> --}}
                        {{-- <td> {{ isset($value->log_name) && $value->log_name != '' ? $value->log_name : 'Not Set' }} </td> --}}
                        <td> {{ isset($value->description) && $value->description != '' ? ' Record ' . $value->description : '-' }}
                        </td>
                        <td> {{ isset($value->causer_id) && $value->causer_id != '' ? $value->causer->first_name . ' ' . $value->causer->last_name : 'Not Set' }}
                        </td>
                        <td>
                            @if (isset($value->changes['old']) && !empty($value->changes['old']) && isset($value->changes['attributes']) && !empty($value->changes['attributes']))
                                <table class="table table-bordered table-sm ">
                                    <thead>
                                        <tr>
                                            <th scope="col">{{ trans('activity.lbl_activity_field_name') }}</th>
                                            <th scope="col">{{ trans('activity.lbl_activity_old_value') }}</th>
                                            <th scope="col">{{ trans('activity.lbl_activity_new_value') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $old_change_array = $value->changes['old'];
                                            $new_change_array = $value->changes['attributes'];
                                        @endphp
                                        @foreach ($old_change_array as $sub_key => $sub_value)
                                            <tr>
                                                @switch($sub_key)
                                                    @case('brand_id')
                                                        @php
                                                            $_brand_old_value = \App\Models\Brands::withTrashed()->find($sub_value);
                                                            $_brand_new_value = \App\Models\Brands::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_brand_old_value->brand_name }}</td>
                                                        <td>{{ $_brand_new_value->brand_name }}</td>
                                                    @break
                                                    @case('brand_model_id')
                                                        @php
                                                            $_brand_model_old_value = \App\Models\BrandModels::withTrashed()->find($sub_value);
                                                            $_brand_model_new_value = \App\Models\BrandModels::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_brand_model_old_value->model_name }}</td>
                                                        <td>{{ $_brand_model_old_value->model_name }}</td>
                                                    @break
                                                    @case('role_id')
                                                        @php
                                                            $_role_old_value = \App\Models\Roles::withTrashed()->find($sub_value);
                                                            $_role_new_value = \App\Models\Roles::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_role_old_value->name }}</td>
                                                        <td>{{ $_role_new_value->name }}</td>
                                                    @break
                                                    @case('marital_status')
                                                        @php
                                                            $marital_status = $Common::getMaritalStatus();                       
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ array_key_exists($sub_value, $marital_status) ? $marital_status[$sub_value] : $sub_value }}</td>
                                                        <td>{{ array_key_exists($new_change_array[$sub_key],$marital_status) ? $marital_status[$new_change_array[$sub_key]] : $new_change_array[$sub_key] }}
                                                        </td>
                                                    @break
                                                    @case('education')
                                                        @php
                                                            $education = $Common::getEducation();
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ array_key_exists($sub_value, $education) ? $education[$sub_value] : $sub_value }}</td>
                                                        <td>{{ array_key_exists($new_change_array[$sub_key],$education) ? $education[$new_change_array[$sub_key]] : $new_change_array[$sub_key] }}
                                                        </td>
                                                    @break
                                                    @case('occupation')
                                                        @php
                                                            $occupation = $Common::getOccupation();                       
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ array_key_exists($sub_value, $occupation) ? $occupation[$sub_value] : $sub_value }}</td>
                                                        <td>{{ array_key_exists($new_change_array[$sub_key],$occupation) ? $occupation[$new_change_array[$sub_key]] : $new_change_array[$sub_key] }}
                                                        </td>
                                                    @break
                                                    @case('ownership')
                                                        @php
                                                            $ownership = $Common::getOwnerships();
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ array_key_exists($sub_value, $ownership) ? $ownership[$sub_value] : $sub_value }}</td>
                                                        <td>{{ array_key_exists($new_change_array[$sub_key],$ownership) ? $ownership[$new_change_array[$sub_key]] : $new_change_array[$sub_key] }}
                                                        </td>
                                                    @break
                                                    @case('driven_by')
                                                        @php
                                                            $driven_by = $Common::getDrivenBy();
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ array_key_exists($sub_value, $driven_by) ? $driven_by[$sub_value] : $sub_value }}</td>
                                                        <td>{{ array_key_exists($new_change_array[$sub_key],$driven_by) ? $driven_by[$new_change_array[$sub_key]] : $new_change_array[$sub_key]
                                                             }} </td>
                                                    @break    
                                                    @case('vehicle_type')
                                                        @php
                                                            $vehicle_type = $Common::getVehicleType();
                                                        @endphp
                                                            <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                            <td>{{ array_key_exists($sub_value, $vehicle_type) ? $vehicle_type[$sub_value] : $sub_value }}</td>
                                                            <td>{{ array_key_exists($new_change_array[$sub_key],$vehicle_type) ? $vehicle_type[$new_change_array[$sub_key]] : $new_change_array[$sub_key] }} </td>
                                                    @break
                                                    @case('licence_type')
                                                        @php
                                                            $licence_type = $Common::getLicenceType();

                                                            $sub_value_data = "";
                                                            if(isset($sub_value)){
                                                                $sub_value_datas = explode(",",$sub_value);
                                                                foreach($sub_value_datas as $key => $v){
                                                                      $sub_value_data .=  $licence_type[$v] . ', ';
                                                                }
                                                            }else{
                                                                $sub_value_data = "";
                                                            }

                                                            $new_sub_key = "";
                                                            if(isset($new_change_array[$sub_key])){
                                                                $new_sub_keys = explode(",",$new_change_array[$sub_key]);
                                                                foreach($new_sub_keys as $key => $v){
                                                                    $new_sub_key .= $licence_type[$v] . ', ';
                                                                }
                                                            }else{
                                                                $new_sub_key = "";
                                                            }
                                                        @endphp
                                                            <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                            <td>{{ $sub_value_data }}</td>
                                                            <td>{{ $new_sub_key }}</td>  
                                                    @break
                                                    @case('disability')
                                                        @php                   
                                                            $disability = 
                                                            $Common::getDisability();            
                                                        @endphp               
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ array_key_exists($sub_value, $disability) ? $disability[$sub_value] : $sub_value }}</td>
                                                        <td>{{ array_key_exists($new_change_array[$sub_key],$disability) ? $disability[$new_change_array[$sub_key]] : $new_change_array[$sub_key] }}</td>
                                                    @break
                                                    @case('status')
                                                        @php                   
                                                            $status = $Common::getStatus();   
                                                        @endphp               
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ array_key_exists($sub_value, $status) ? $status[$sub_value] : $sub_value }}</td>
                                                        <td>{{ array_key_exists($new_change_array[$sub_key],$status) ? $status[$new_change_array[$sub_key]] : $new_change_array[$sub_key] }} </td>
                                                    @break
                                                    @case('group_id')
                                                        @php
                                                            $_group_old_value = \App\Models\Groups::withTrashed()->find($sub_value);
                                                            $_group_new_value = \App\Models\Groups::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_group_old_value->name }}</td>
                                                        <td>{{ $_group_new_value->name }}</td>
                                                    @break
                                                    @case('gender')
                                                        @php
                                                            $_group_old_value = $Common::getGender($sub_value);
                                                            $_group_new_value = $Common::getGender($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_group_old_value }}</td>
                                                        <td>{{ $_group_new_value }}</td>
                                                    @break
                                                    @case('city_id')
                                                        @php
                                                            $_group_old_value = \App\Models\City::withTrashed()->find($sub_value);
                                                            $_group_new_value = \App\Models\City::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_group_old_value->city_name }}</td>
                                                        <td>{{ $_group_new_value->city_name }}</td>
                                                    @break
                                                    @case('state_id')
                                                        @php
                                                            $_group_old_value = \App\Models\State::withTrashed()->find($sub_value);
                                                            $_group_new_value = \App\Models\State::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_group_old_value->state_name }}</td>
                                                        <td>{{ $_group_new_value->state_name }}</td>
                                                    @break 
                                                    @case('country_id')
                                                        @php
                                                            $_country_old_value = \App\Models\Country::withTrashed()->find($sub_value);
                                                            $_country_new_value = \App\Models\Country::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_country_old_value->country_name }}</td>
                                                        <td>{{ $_country_new_value->country_name }}</td>
                                                    @break            
                                                    @case('vehicle_id')
                                                        @php
                                                            $_vehicle_old_value = \App\Models\Vehicle::withTrashed()->find($sub_value);
                                                            $_vehicle_new_value = \App\Models\Vehicle::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_vehicle_old_value->registration_number }}</td>
                                                        <td>{{ $_vehicle_new_value->registration_number }}</td>
                                                    @break
                                                    @case('driver_id')
                                                        @php
                                                            $_driver_old_value = \App\Models\Driver::withTrashed()->find($sub_value);
                                                            $_driver_new_value = \App\Models\Driver::withTrashed()->find($new_change_array[$sub_key]);
                                                        @endphp
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $_driver_old_value->full_name }}</td>
                                                        <td>{{ $_driver_new_value->full_name }}</td>
                                                    @break
                                                    @default
                                                        <td>{{ $Common::getAttributeName($sub_key) }}</td>
                                                        <td>{{ $sub_value }}</td>
                                                        <td>{{ $new_change_array[$sub_key] }}
                                                        </td>
                                                @endswitch
                                                {{-- @if (isset($sub_key) && $sub_key == 'status')
                                                <td>{{ $sub_key }}</td>
                                                <td>{{ App\Utility\Common::getStatus($sub_value) }}</td>
                                                <td>{{ App\Utility\Common::getStatus($new_change_array[$sub_key]) }} </td>
                                            @else
                                                <td>{{ $sub_key }}</td>
                                                <td>{{ $sub_value }}</td>
                                                <td>{{ $new_change_array[$sub_key] }}</td>
                                            @endif --}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                {{ 'No change' }}
                            @endif
                        </td>
                        <td> {{ isset($value->updated_at) && $value->updated_at != '' ? App\Utility\Common::setDateTimeFormat($value->updated_at) : 'Not Set' }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer">
        <div class="">
            @include('layouts._component._pagination', ['pagination' => $activity_data])
        </div>
    </div>
@else
<div class="card-body table-responsive">    
        {{ trans('common.lbl_data_not_found') }}
    </div>
@endif
