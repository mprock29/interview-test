@php
    $action = app('request')->route()->getAction();
    $uri = app('request')->route()->uri;
@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ url('/') }}" class="brand-link " style="background: white;color: #4b545c;">
        <img src="{{ asset('img/logo.png') }}" alt="Yhonk" class="brand-image " style="opacity: .8">
        {{-- img-circle elevation-3--}}
            <span class="brand-text font-weight-light">Admin {{-- config('app.name', 'Yhonk') --}} </span>
    </a>

    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                @if (isset($action['controller']))
                @php
                    $controller = class_basename($action['controller']);
                    list($controller, $action) = explode('@', $controller);
                @endphp
                @else
                @php
                    $parametersArray = app('request')->route()->parameters();
                    $controller = $parametersArray['controller'];
                @endphp
                @endif
                @php
                    $menusArr = \App\Utility\Common::sideBarDesign();
                @endphp

                @if (isset($menusArr) && !empty($menusArr))
                    @foreach ($menusArr as $key => $value)
                        @php
                            $route = !empty($value['url']) ? $value['url'] : 'home';
                            $menuIcon = !empty($value['menu_icon']) ? $value['menu_icon'] : '-';
                        @endphp
                        @if(!isset($value['children']) && empty($value['children']))
                            <li class="nav-item">
                                <a href="{{ ($route != '#') ? route($route) : '' }}" class="nav-link {{ $value['active'] == 1 ? 'active' : null }}">
                                    <i class=" {{ $menuIcon }}  "></i>
                                        <p class="text"> {{ $value['menu_title'] }}</p>
                                </a>
                            </li>
                        @endif

                        @if (isset($value['children']) && !empty($value['children']))
                            @php
                                $urlMatch = array_column($value['children'], 'url');
                                $currenturlactiveClass = in_array(Request::route()->getName(), $urlMatch);
                            @endphp

                            <li class="nav-item has-treeview {{ ($currenturlactiveClass == 1 || $value['active'] == 1) ? 'active menu-open' : null }}">
                                <a href="javascript:void(0)" class="nav-link {{ ($currenturlactiveClass == 1  || $value['active'] == 1) ? 'active' : null }}">
                                    <i class="{{ $menuIcon }}"></i>
                                        <p class="text">  {{ $value['menu_title'] }}
                                            <i class="right fas fa-angle-left"></i>
                                        </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @foreach ($value['children'] as $key => $val)
                                        @php
                                            $activeChild = !empty($val['active']) ? $val['active'] : 0;
                                            $routeChild = !empty($val['url']) ? $val['url'] : 'home';
                                            $menuIconChild = !empty($val['menu_icon']) ? $val['menu_icon'] : '-';
                                        @endphp
                                        <li class="nav-item {{ $activeChild == 1 ? 'active' : null }}">
                                            <a href="<?php echo ($routeChild != '#') ? route($routeChild) : 'javascript:void(0)' ?>" class="nav-link {{ $activeChild == 1 ? 'active' : null }}">
                                                <i class="{{ $menuIconChild }}"></i>
                                                <p class="text"><?= $val['menu_title'] ?></p>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
        </nav>
    </div>
</aside>
