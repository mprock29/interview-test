<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Yhonk') }} - @yield('title')</title>
    <link rel="icon" href="/img/logo.png" type="image/x-icon"/>

    <!-- Theme style -->
    <link rel="stylesheet" href="/css/adminlte.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/all.min.css">
    <link rel="stylesheet" href="/css/custome.css">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="/css/icheck-bootstrap.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="/css/toastr.min.css"> <!-- or use the @toastr_css-->
    <link rel="stylesheet" href="/css/select2.min.css">
    <link rel="stylesheet" href="/css/select2-bootstrap4.min.css">
    <!-- Jstree -->
    <link rel="stylesheet" href="/css/jstree.min.css">
    <!-- daterangepicker-->
    <link rel="stylesheet" href="/css/daterangepicker.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/ion-rangeslider/css/ion.rangeSlider.min.css">

</head>
<body >
    <div id="app">
        <div class="wrapper">
           
            @yield('content')
           
        </div>
    </div>
    <!-- jQuery -->
    <script src="/js/jquery.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/js/adminlte.js"></script>
    <!-- Bootstrap 4 -->
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="/js/toastr.min.js"></script> <!-- or use the @toastr_js -->
    @toastr_render
    <script src="/js/select2.min.js"></script>
    <script src="/js/jstree.min.js"></script>
    <!-- daterangepicker-->
    <script src="/js/moment.min.js"></script>
    <script src="/js/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="/js/jquery-ui.js"></script>
    <script src="/js/dashboard.js"></script>
    <script src="/js/bootstrap-switch.js"></script>
    <script src="{{ asset('node_modules/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <!-- Custome JS-->
    <script src="/js/custome.js"></script>
    <script src="https://adminlte.io/themes/v3/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
    @yield('script')
</body>
</html>
