<!-- PRODUCT LIST -->
<div class="card">
    <div class="overlay user-login-list-loading">
        <i class="fas fa-3x fa-spin fa-sync-alt"></i>
    </div>
    <div class="card-header">
        <h3 class="card-title">Recently Logged In Users</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        <ul class="products-list product-list-in-card pl-2 pr-2">            
            @foreach ($usersLoginHistory as $item)            
                <li class="item">
                    <div class="product-img">
                       @php
                            if(!empty($item->profile_pic)) {
                                $URL = $item->profile_pic;
                            } else {
                                $URL = asset('/uploads/common/avatar.png');
                            }
                        @endphp
                        <img src="{{$URL}}" alt={{$item->username}} class="img-size-50">
                    </div>
                    <div class="product-info">
                        <a href="javascript:void(0)" class="product-title pointer-events-none">{{$item->first_name}} {{$item->last_name}}                            
                            <span class="badge badge-secondary float-right">{{$item->rolesWithTrashed->name}}</span></a>
                        <span class="product-description">
                            @if (!empty($item->userLoginHistory->login_at) && !empty($item->userLoginHistory->logout_at))
                            @php
                                $origin = new DateTime($item->userLoginHistory->login_at);
                                $target = new DateTime($item->userLoginHistory->logout_at);
                                $diff_mins = abs($origin->getTimestamp() - $target->getTimestamp()) / 60;
                            @endphp
                                Login Duration : {{round($diff_mins)}} Minutes
                            @else
                                Currently logged-In
                            @endif
                        </span>
                    </div>
                </li>
            @endforeach

        </ul>
    </div>
    <!-- /.card-body -->
    @if(\App\Utility\Common::checkActionAccess("users.view"))
        <div class="card-footer text-center">
            <a href="{{route('users.view')}}" class="uppercase">View All Login History</a>
        </div>
    @endif
    <!-- /.card-footer -->
</div>
<!-- /.card -->
