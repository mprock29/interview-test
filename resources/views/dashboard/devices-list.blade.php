<div class="card">
    <div class="overlay device-list-loading">
        <i class="fas fa-3x fa-spin fa-sync-alt"></i>
    </div>
    <div class="card-header border-transparent">
        <h3 class="card-title">Connected Device List</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table m-0">
                <thead>
                    <tr>
                        <th>{{ trans('device.lbl_group_id') }}</th>
                        <th>{{ trans('device.lbl_imei_number') }}</th>
                        <th>{{ trans('device.lbl_vehicle_id') }}</th>
                        <th>{{ trans('device.lbl_driver_id') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($devices) > 0)
                        @foreach ($devices as $device)
                            <tr>
                                <td>{{ $device->deviceRelation->groupWithTrashed->name }}</td>
                                <td>{{ $device->deviceRelation->imei_number }}</td>
                                <td>{{ isset($device->vehicleRelationWithTrash->registration_number) ? $device->vehicleRelationWithTrash->registration_number : trans('device.lbl_NOT_ASSIGNED') }}</td>
                                <td>{{ isset($device->driverRelationWithTrash->full_name) ? $device->driverRelationWithTrash->full_name : trans('device.lbl_NOT_ASSIGNED') }} </td>
                            </tr>
                        @endforeach
                        @else
                        <tr> <td>{{ trans('device.No_Devices_Connected') }}</td></tr>
                    @endif
                </tbody>
            </table>
        </div>
        <!-- /.table-responsive -->
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <a href="{{ route('devices') }}" class="btn btn-sm btn-secondary float-right">View All Devices</a>
    </div>
    <!-- /.card-footer -->
</div>
