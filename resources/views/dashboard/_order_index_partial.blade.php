@csrf
@if ($orders->isNotEmpty())
<div class="container-fluid">
<div class="card-body">
    <div class="table-responsive">
    <table class=" table table-bordered table-hover table-sm ">
        <thead>
            <tr style="border-top:1px solid #dee2e6;">
                <th scope="col">{{ trans('orders.lbl_unique_order_id') }}</th>
                <th scope="col">{{ trans('orders.lbl_user_id') }}</th>
                <th scope="col">{{ trans('common.lbl_status') }}</th>
                <th scope="col">{{ trans('orders.lbl_order_date') }} </th>
                <th scope="col">{{ trans('common.lbl_action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $key => $value)
            <tr>
                <td size="5"> {{ $value->unique_order_id }}</td>
                <td> {{ isset($value->getUserDetail) && !empty($value->getUserDetail) ? $value->getUserDetail->full_name : "Not Set" }}</td>
                <td><small class="badge badge-success">{{ App\Utility\Common::getOrderStatus($value->status) }}</small></td>
                <td> {{ isset($value->created_at) && $value->created_at != '' ? App\Utility\Common::setDateTimeFormat($value->created_at) : 'Not Set' }}</td>
                <td class="text-nowrap" style="text-align: center">
                    @if (\App\Utility\Common::checkActionAccess('orders.view'))
                    <a target="_blank" class="text-info text-lg" href="{{ route('orders.view', ['orders' => $value->id]) }}" title="{{ trans('roles.lbl_orders_view') }}">
                        <span> <i class="fas fa-eye" title="{{ trans('roles.lbl_orders_view') }}"></i> </span>
                    </a>
                    @endif
                    @if (\App\Utility\Common::checkActionAccess('orders.edit'))
                    <a target="_blank" class="text-warning text-lg" href="{{ route('orders.edit', ['orders' => $value->id]) }}" title="{{ trans('orders.lbl_assign') }}">
                        <span> <i class="fas fa-table" title="{{ trans('orders.lbl_assign') }}"></i> </span>
                    </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
<div id="order_count_dashboard" style="display: none;"> {{ $order_count }}</div>
@else
<div class="card-body table-responsive">
    {{ trans('common.lbl_data_not_found') }}
</div>
@endif