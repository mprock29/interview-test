<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="/css/adminlte.min.css">
  <link rel="stylesheet" href="/css/all.min.css">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/adminlte.js"></script>   
  <script src="/js/bootstrap.bundle.min.js"></script>  

   <style type="text/css">
      <style type="text/css">               
        html {
            line-height: 1.15;
            -webkit-text-size-adjust: 100%
        }

        .justify-center {
            justify-content: center
        }

        .relative {
            position: relative
        }
        
        .text-center {
            text-align: center
        }
        body{
          margin: 0;
          font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol";
          font-size: 1rem;
          font-weight: 400;
          line-height: 1.5;
          color: #212529;
          text-align: left;
          background-color: #fff;
        }
       div .error-page {      
          margin-top: 170px;
      }
      p{
        margin-top: 0;
        margin-bottom: 1rem;
      }
    </style>
</head>
<body>
  <div class="container-fluid">               
      <div class="error-page relative justify-center">
        <h1 class="headline text-warning">404</h1>
          <div class="error-content">
            <h3>
              <i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.
            </h3>
            <p>We could not find the page you were looking for.</p>
            <a class="text-center" href="{{ route('home') }}">Return to Dashboard.</a>
          </div>        
        </div>           
  </div>
</body>
</html>
