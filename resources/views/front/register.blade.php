@extends('layouts.front')
@section('title', 'register')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{route('front.user.register.submit')}}">
                        @csrf

                        <div class="row mb-3">
                            <label for="first_name" class="col-md-4 col-form-label text-md-end">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}"  autocomplete="first_name" autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="last_name" class="col-md-4 col-form-label text-md-end">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}"  autocomplete="last_name" autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="date_of_birth" class="col-md-4 col-form-label text-md-end">{{ __('Date of Birth') }}</label>

                            <div class="col-md-6">
                                <input id="date_of_birth" type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ old('date_of_birth') }}"  autocomplete="date_of_birth" autofocus>

                                @error('date_of_birth')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="gender" class="col-md-4 col-form-label text-md-end">{{ __('Gender') }}</label>

                            <div class="col-md-6">
                                <input type="radio" id="male" name="gender" value="male" checked>
                                <label for="male">Male</label><br>
                                <input type="radio" id="female" name="gender" value="female">
                                <label for="female">Female</label><br>

                                @error('gender')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="annual_income" class="col-md-4 col-form-label text-md-end">{{ __('Annual Income') }}</label>

                            <div class="col-md-6">
                                <input id="annual_income" type="text" class="form-control @error('annual_income') is-invalid @enderror" name="annual_income" value="{{ old('annual_income') }}"  autocomplete="annual_income" autofocus>

                                @error('annual_income')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="occupation" class="col-md-4 col-form-label text-md-end">{{ __('Occupation') }}</label>

                            <div class="col-md-6">
                            <select name="occupation" id="occupation" class="form-control">
                                <option value="">Select Occupation</option>
                                <option value="Private Job">Private Job</option>
                                <option value="Government Job">Government Job</option>
                                <option value="Business">Business</option>                                
                            </select>

                                @error('occupation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="family_type" class="col-md-4 col-form-label text-md-end">{{ __('Family Type') }}</label>

                            <div class="col-md-6">
                            <select name="family_type" id="family_type" class="form-control">
                                <option value="">Select Family Type</option>
                                <option value="Joint Family">Joint Family</option>
                                <option value="Nuclear Family">Nuclear Family</option>                                                            
                            </select>

                                @error('family_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="row mb-3">
                            <label for="manglik" class="col-md-4 col-form-label text-md-end">{{ __('Manglik') }}</label>

                            <div class="col-md-6">
                            <select name="manglik" id="manglik" class="form-control">
                                <option value="">Select Manglik</option>
                                <option value="Yes">Yes</option>
                                <option value="no">No</option>                                                         
                            </select>

                                @error('manglik')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                       
                    <div class="card">
                        <div class="card-header">{{ __('Partner Preference') }}</div>
                        <div class="card-body">
                        <div class="row mb-3">
                            <label for="partner_expected_income" class="col-md-4 col-form-label text-md-end">{{ __('Occupation') }}</label>

                            <div class="col-md-6">
                            <input id="partner_expected_income" type="text" name="partner_expected_income" value="">
                                @error('partner_expected_income')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="partner_occupation" class="col-md-4 col-form-label text-md-end">{{ __('Partner occupation') }}</label>

                            <div class="col-md-6">
                            <select name="partner_occupation[]" id="partner_occupation" class="form-control" multiple>
                                
                                <option value="Private Job">Private Job</option>
                                <option value="Government Job">Government Job</option>
                                <option value="Business">Business</option>                                
                            </select>

                                @error('partner_occupation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="partner_family_type" class="col-md-4 col-form-label text-md-end">{{ __('Partner family type') }}</label>

                            <div class="col-md-6">
                            <select name="partner_family_type[]" id="partner_family_type" class="form-control" multiple>
                                
                            
                                <option value="Joint Family">Joint Family</option>
                                <option value="Nuclear Family">Nuclear Family</option>                                                            
                            </select>

                                @error('partner_family_type')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="partner_manglik" class="col-md-4 col-form-label text-md-end">{{ __('Partner Manglik') }}</label>

                            <div class="col-md-6">
                            <select name="partner_manglik" id="partner_manglik" class="form-control">
                            <option value="Yes">Yes</option>
                                <option value="no">No</option>                                                         
</select>

                                @error('partner_manglik')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        
    
                        </div>
                    </div>
                    <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
        <script type="text/javascript">
            $('#family_type').select2();
            $('#occupation').select2();
            $('#manglik').select2();
            $('#partner_family_type').select2();
            $('#partner_occupation').select2();
            $('#partner_manglik').select2();
            $('#partner_expected_income').ionRangeSlider({
                min     : 50000,
                max     : 500000,
                from    : 100000,
                to      : 400000,
                type    : 'double',
                step    : 1,
                prefix  : 'Rs',
                prettify: false,
                hasGrid : true
                })
        </script>
    @endsection


