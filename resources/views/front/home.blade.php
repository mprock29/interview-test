@extends('layouts.front')
@section('title', 'Dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h4>Welcome</div>

                    <div class="card-body">

                        <div class="row">

                           
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4><a href="{{route('front.login')}}">Login</a></h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4><a href="{{route('front.user.register')}}">Register</a></h4>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        @php
                            //    dd($user_data);
                        @endphp
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
