@extends('layouts.front')
@section('title', 'Dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h4>Dashboard : Welcome {{auth()->user()->first_name}} {{auth()->user()->last_name}}</h4> <a class="btn btn-danger float-right" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                            {{ trans('common.lbl_logout') }}
                        </a> <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form></div>

                    <div class="card-body">

                        <div class="row">

                            @foreach ($user_data as $user)
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-header bg-info text-white">
                                            <h4>{{ $user['first_name'] }} {{ $user['last_name'] }}</h4>
                                        </div>
                                        <div class="card-body">
                                            <ul>
                                                <li>Percentage Match : {{$user['percentage']}} %</li>
                                                <li>Gender : {{$user['gender']}}</li>
                                                <li>Date of birth : {{$user['date_of_birth']}}</li>
                                                <li>Occupation : {{$user['occupation']}}</li>
                                                <li>Email : {{$user['email']}}</li>
                                                <li>Family Type : {{$user['family_type']}}</li>
                                                <li>Manglik : {{$user['manglik']}}</li>
                                                <li>Annual Income : {{$user['annual_income']}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        @php
                            //    dd($user_data);
                        @endphp
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
