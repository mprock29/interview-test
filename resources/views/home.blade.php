@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">User List
                           </button>
                        </h1>
                    </div>

                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-sm">
                                        <thead>
                                            <tr>
                                                <th>Firstname</th>
                                                <th>Lastname</th>
                                                <th>Email</th>
                                                <th>Date of birth</th>
                                                <th>Annual Income</th>
                                                <th>Occupation</th>
                                                <th>Family type</th>
                                                <th>Manglik</th>
                                                <th>Gender</th>
                                                <th>Partner expected income</th>
                                                <th>Partner occupation</th>
                                                <th>Partner family type</th>
                                                <th>Partner manglik</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($user_data as $user)
                                            <tr>
                                                <td>{{$user->first_name}}</td>
                                                <td>{{$user->last_name}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->date_of_birth}}</td>
                                                <td>{{$user->annual_income}}</td>
                                                <td>{{$user->occupation}}</td>
                                                <td>{{$user->family_type}}</td>
                                                <td>{{$user->manglik}}</td>
                                                <td>{{$user->gender}}</td>
                                                <td>{{$user->partner_expected_income}}</td>
                                                <td>{{$user->partner_occupation}}</td>
                                                <td>{{$user->partner_family_type}}</td>
                                                <td>{{$user->partner_manglik}}</td>
                                            </tr>
                                                
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
@endsection
