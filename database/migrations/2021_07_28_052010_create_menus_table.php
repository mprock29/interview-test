<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('menus', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('parent_id')->unsigned()->comment('Reference of Menus Table')->nullable();
			$table->bigInteger('child_menu_id')->unsigned()->comment('Reference of Menus Table')->nullable();
			$table->string('menu_title', 255)->nullable();
			$table->string('page_url', 255)->nullable();
			$table->boolean('show_in_menu')->comment('0 => No, 1 => Yes')->defult(0)->nullable();
			$table->bigInteger('sort_order')->nullable();
			$table->string('menu_icon', 255)->nullable();
			$table->boolean('status')->comment('0 => Inactive, 1 => Active')->defult(0)->nullable();
			$table->timestamps();
			$table->bigInteger('created_by')->nullable();
			$table->bigInteger('updated_by')->nullable();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('menus');
	}
}
