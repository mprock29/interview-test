<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('role_id')->comment('Reference of Roles Table')->nullable();
            $table->bigInteger('menu_id')->comment('Reference of Menus Table')->nullable();
            $table->boolean('status')->defult(1)->comment('0 => Inactive, 1 => Active')->nullable(); 
            $table->timestamps();
            $table->bigInteger('created_by')->comment('Reference of user table')->nullable();
            $table->bigInteger('updated_by')->comment('Reference of user table')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {    
        Schema::dropIfExists('roles_permissions');
    }
}
