<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->boolean('status')->comment('0 => Inactive, 1 => Active , 2 => Deleted'); //Data type is tinyInteger with 1 length
            $table->bigInteger('created_by')->nullable()->unsigned()->comment('Reference of users table');
            $table->bigInteger('updated_by')->nullable()->unsigned()->comment('Reference of users table');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*Drop Foreign Key*/
        Schema::table('roles', function (Blueprint $table) {

            $table->dropForeign(['updated_by']);
            $table->dropForeign(['created_by']);
            $table->dropSoftDeletes();
        });

        Schema::dropIfExists('roles');
    }
}
