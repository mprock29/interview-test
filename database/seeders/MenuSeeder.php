<?php

namespace Database\Seeders;

use App\Models\Menus;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Menus::query()->forceDelete();
		$menus = array(
			array('id' => '1','parent_id' => '0','child_menu_id' => '0','menu_title' => 'User Data','page_url' => 'home','show_in_menu' => '1','sort_order' => '30','menu_icon' => 'fas fa-tachometer-alt','status' => '1','created_at' => '2021-08-10 05:24:35','updated_at' => '2021-08-10 05:24:35','created_by' => '1','updated_by' => '1','deleted_at' => NULL),
			// array('id' => '7','parent_id' => '2','child_menu_id' => '0','menu_title' => 'Menus','page_url' => 'menus','show_in_menu' => '0','sort_order' => '54','menu_icon' => 'far fa-user','status' => '1','created_at' => '2021-08-10 05:38:26','updated_at' => '2021-09-16 05:04:56','created_by' => '1','updated_by' => '1','deleted_at' => NULL),
			// array('id' => '8','parent_id' => '2','child_menu_id' => '7','menu_title' => 'Create Menu','page_url' => 'menus.create','show_in_menu' => '0','sort_order' => '55','menu_icon' => 'fa fa-plus','status' => '1','created_at' => '2021-08-10 05:40:02','updated_at' => '2021-08-10 05:40:02','created_by' => '1','updated_by' => '1','deleted_at' => NULL),
			// array('id' => '9','parent_id' => '2','child_menu_id' => '7','menu_title' => 'Edit Menu','page_url' => 'menus.edit','show_in_menu' => '0','sort_order' => '56','menu_icon' => 'fas fa-edit','status' => '1','created_at' => '2021-08-10 05:41:43','updated_at' => '2021-08-10 05:41:43','created_by' => '1','updated_by' => '1','deleted_at' => NULL),
			// array('id' => '10','parent_id' => '2','child_menu_id' => '7','menu_title' => 'Delete Menu','page_url' => 'menus.delete','show_in_menu' => '0','sort_order' => '57','menu_icon' => 'fas fa-trash','status' => '1','created_at' => '2021-08-10 05:42:42','updated_at' => '2021-08-11 04:24:39','created_by' => '1','updated_by' => '1','deleted_at' => NULL),
			
		  );		  
		Menus::insert($menus);
	}
}
