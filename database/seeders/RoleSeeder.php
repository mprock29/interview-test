<?php

namespace Database\Seeders;

use App\Models\Roles;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		Roles::query()->forceDelete();
		$roles = array(
			array('id' => '1', 'name' => 'Super Admin', 'status' => '1', 'created_by' => 1, 'updated_by' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'deleted_at' => NULL),
			array('id' => '2', 'name' => 'Front User', 'status' => '1', 'created_by' => 1, 'updated_by' => 1, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'deleted_at' => NULL),
			
		);
		Roles::insert($roles);
	}
}
