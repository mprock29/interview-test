<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->forceDelete();
        $users = array(
            array('id' => '1', 'role_id' => '1', 'first_name' => 'Super', 'last_name' => 'Admin', 'email' => 'admin@admin.com', 'email_verified_at' => null, 'password' => '$2y$10$qT.zSbscQ8T2uMXykLiO5e2CmTzdqWjP1f8lTGjIxokB4X6xdLbBu', 'date_of_birth' => null, 'gender' => null, 'annual_income' => null, 'occupation' => null, 'family_type' => null, 'manglik' => null, 'partner_expected_income' => null, 'partner_occupation' => null, 'partner_family_type' => null, 'partner_manglik' => null, 'remember_token' => null, 'created_at' => '2022-03-10 20:56:26', 'updated_at' => '2022-03-10 20:56:26', 'deleted_at' => null),
            array('id' => '2', 'role_id' => '2', 'first_name' => 'user', 'last_name' => '1', 'email' => 'mprock29@live.com', 'email_verified_at' => null, 'password' => '$2y$10$Su6HCB/uyW.hnVlSE4hv4uCDZxU5xqemCN8wDTXWROhauAYMkQUzu', 'date_of_birth' => '1992-06-29', 'gender' => 'male', 'annual_income' => '400000', 'occupation' => 'Private Job', 'family_type' => 'Joint Family', 'manglik' => 'no', 'partner_expected_income' => '100000;500000', 'partner_occupation' => 'Government Job,Business', 'partner_family_type' => 'Joint Family,Nuclear Family', 'partner_manglik' => 'no', 'remember_token' => null, 'created_at' => '2022-03-10 21:26:15', 'updated_at' => '2022-03-10 21:26:15', 'deleted_at' => null),
            array('id' => '3', 'role_id' => '2', 'first_name' => 'user', 'last_name' => '2', 'email' => 'user2@live.com', 'email_verified_at' => null, 'password' => '$2y$10$Su6HCB/uyW.hnVlSE4hv4uCDZxU5xqemCN8wDTXWROhauAYMkQUzu', 'date_of_birth' => '1993-06-29', 'gender' => 'female', 'annual_income' => '400000', 'occupation' => 'Private Job', 'family_type' => 'Joint Family', 'manglik' => 'yes', 'partner_expected_income' => '100000;500000', 'partner_occupation' => 'Private Job,Government Job,Business', 'partner_family_type' => 'Joint Family,Nuclear Family', 'partner_manglik' => 'yes', 'remember_token' => null, 'created_at' => '2022-03-10 21:26:15', 'updated_at' => '2022-03-10 21:26:15', 'deleted_at' => null),
            array('id' => '4', 'role_id' => '2', 'first_name' => 'user', 'last_name' => '3', 'email' => 'user3live.com', 'email_verified_at' => null, 'password' => '$2y$10$Su6HCB/uyW.hnVlSE4hv4uCDZxU5xqemCN8wDTXWROhauAYMkQUzu', 'date_of_birth' => '1991-06-29', 'gender' => 'male', 'annual_income' => '400000', 'occupation' => 'Private Job', 'family_type' => 'Joint Family', 'manglik' => 'yes', 'partner_expected_income' => '100000;500000', 'partner_occupation' => 'Private Job,Government Job,Business', 'partner_family_type' => 'Joint Family,Nuclear Family', 'partner_manglik' => 'no', 'remember_token' => null, 'created_at' => '2022-03-10 21:26:15', 'updated_at' => '2022-03-10 21:26:15', 'deleted_at' => null),
            array('id' => '5', 'role_id' => '2', 'first_name' => 'user', 'last_name' => '4', 'email' => 'user4@live.com', 'email_verified_at' => null, 'password' => '$2y$10$Su6HCB/uyW.hnVlSE4hv4uCDZxU5xqemCN8wDTXWROhauAYMkQUzu', 'date_of_birth' => '1994-06-29', 'gender' => 'female', 'annual_income' => '400000', 'occupation' => 'Government Job', 'family_type' => 'Joint Family', 'manglik' => 'yes', 'partner_expected_income' => '100000;500000', 'partner_occupation' => 'Private Job,Government Job,Business', 'partner_family_type' => 'Joint Family,Nuclear Family', 'partner_manglik' => 'no', 'remember_token' => null, 'created_at' => '2022-03-10 21:26:15', 'updated_at' => '2022-03-10 21:26:15', 'deleted_at' => null),
        );
        User::insert($users);
    }
}
