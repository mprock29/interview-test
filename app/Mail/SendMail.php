<?php

namespace App\Mail;

use App\Models\EmailTemplateContent;
// use App\Models\BaseModel;
use App\Models\Languages;
// use App\Models\EmailLogs;
use App\Utility\Common;
use File;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     * @param $mail_data
     * @param null $type
     */
    public function __construct($mail_data, $type = null, $replace_word = array(), $replace_by = array(), $attachmentData = null, $reportType = null)
    {   
        $this->type = $type;
        $this->replace_word = $replace_word;
        $this->user_lang_id = $mail_data['lang_id'];
        $this->replace_by = $replace_by;
        $this->bcc_email = [];
        $this->cc_email = [];
        $this->all_files = [];
        $this->send_to = $mail_data['to_email'];
        // $this->send_to = 'meet.parikh@drcsystems.com';
        $this->site_from_email = \Config::get('mail.from.address');
        $this->site_from_name = \Config::get('mail.from.name');
        $this->attach_path = null;
        $this->reportType = $reportType;
        if ($reportType == null) {
            # code...
            $this->attachmentData = $attachmentData;
            if ($attachmentData != null) {
                $this->attach_path = $attachmentData;
                if (is_file(public_path($this->attach_path))) {
                    $this->attach_path = public_path($this->attach_path);
                } else {
                    $this->attach_path = null;
                }
            } else {
                $this->attach_path = null;
            }
        } else {
            $this->all_files = File::allfiles($attachmentData);
            // dd($this->all_files);
        }
        // dd($this->attach_path);

        $this->subject = '';
        $this->content = $this->prepare_message();
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (isset($this->content) && !empty($this->content) && $this->content != '') {
            if ($this->attach_path != null) {
                $data = file_get_contents($this->attach_path);
                $name = basename($this->attach_path); // To get file name
                // if (File::exists($this->attach_path)) {
                // File::delete($this->attach_path);
                // }
                return $this->from($this->site_from_email, $this->site_from_name)
                    ->to($this->send_to)
                    // ->bcc($this->bcc_email)
                    // ->cc($this->cc_email)
                    ->subject($this->subject)
                    ->attachData($data, $name)
                    ->html($this->content, 'text/html');
            } elseif (count($this->all_files) > 0) {
                $mail_data = $this->from($this->site_from_email, $this->site_from_name)
                    ->to($this->send_to)
                    ->subject($this->subject);
                foreach ($this->all_files as $file) {
                    $get_ext = Common::getFileExtension($this->reportType);

                    if ($get_ext == $file->getExtension()) {
                        // check size 25000000
                        if (File::size($file->getPathname()) <= 25000000) {
                            $data = file_get_contents($file->getPathname());
                            $name = $file->getFilename();
                            $mail_data->attachData($data, $name);
                            // if (File::exists($file->getPathname())) {
                            // File::delete($file->getPathname());
                            // }
                            $this->content = str_replace(['[DOWNLOAD_FILE_LINK]'], '', $this->content);
                        } else {
                            $enx = \Crypt::encrypt($file->getPathname());
                            $enc = route('user.download-file.get', ['file_encrypt' => $enx]);
                            $tag = '<a href="' . route('user.download-file.get', ['file_encrypt' => $enx]) . '" target="_blank" rel="noopener noreferrer">Download Link</a>';
                            $this->content = str_replace(['[DOWNLOAD_FILE_LINK]'], $tag, $this->content);
                        }
                    }
                }
                $mail_data->html($this->content, 'text/html');
                return $mail_data;
            } else {
                return $this->from($this->site_from_email, $this->site_from_name)
                    ->to($this->send_to)
                    // ->bcc($this->bcc_email)
                    // ->cc($this->cc_email)
                    ->subject($this->subject)
                    ->html($this->content, 'text/html');
            }
        } else {
            return $this->html('', 'text/html');
        }
    }

    /**
     * Usage: Prepare message using table object
     * @param $data
     * @return string
     */
    public function prepare_message()
    {
        $language_id = $this->user_lang_id;
        $language_data = optional(Languages::where('id', $language_id)->first())->toArray();
        // $login_url = config('constant.FRONTEND_LOGIN_URL');

        if (!empty($language_data)) {
            $language_code = $language_data['lang_code'];
            // $login_url = str_replace("[language_code]", $language_data['lang_code'], config('constant.FRONTEND_LOGIN_URL'));
        } else {
            $default_language_data = optional(Languages::where('default_lang', 1)->first())->toArray();
            $language_code = $default_language_data['lang_code'];
            // $login_url = str_replace("[language_code]", $default_language_data['lang_code'], config('constant.FRONTEND_LOGIN_URL'));
        }

        $type = $this->type;
        $email_template_content = EmailTemplateContent::where('lang_id', $language_id)->whereHas('EmailTemplate', function ($query) use ($type) {
            $query->where('code', $type)->where('status', 1);
        })->first();
        $message = '';
        $mail_header = '';
        $mail_footer = '';

        array_push($this->replace_word, "../../img/logo.png");
        array_push($this->replace_by, url('img/logo.png'));

        if (isset($email_template_content) && !empty($email_template_content)) {
            $email_contect = $email_template_content['content'];
            $replace_word = $this->replace_word;
            $replace_by = $this->replace_by;
            $this->subject = str_replace($replace_word, $replace_by, $email_template_content['subject']);
            $message = str_replace($replace_word, $replace_by, $email_contect);
        } else {
            $email_template_content = EmailTemplateContent::where('lang_id', Languages::where('default_lang', 1)->first()->id)->whereHas('EmailTemplate', function ($query) use ($type) {
                $query->where('code', $type)->where('status', 1);
            })->first();
            if (isset($email_template_content) && !empty($email_template_content)) {
                $email_contect = $email_template_content['content'];
                $replace_word = $this->replace_word;
                $replace_by = $this->replace_by;
                $this->subject = str_replace($replace_word, $replace_by, $email_template_content['subject']);
                $message = str_replace($replace_word, $replace_by, $email_contect);
            }
        }

        // $mail_header = Storage::disk('local')->get('mail_header.php');
        // $mail_header = file_get_contents(resource_path('views/mail_header.blade.php'));
        // $mail_footer = file_get_contents(resource_path('views/mail_footer.blade.php'));
        // $message = $mail_header . $message . $mail_footer;
        // dd($message);
        return $message;
    }
}
