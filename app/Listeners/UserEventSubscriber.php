<?php

namespace App\Listeners;
use Request;

class UserEventSubscriber {
	private $UsersLoginHistory;

/**
 * Create the event listener.
 *
 * @return void
 */
	public function __construct(Request $request) {
		$this->request = $request;
	}

	/**
	 * Handle user login events.
	 */
	public function onUserLogin($event) {
		// $user = $event->user;
		// $user->last_login = date('Y-m-d H:i:s');
		// // $user->last_ip = request()->getClientIp();
		// $user->last_ip = (request()->server('SERVER_ADDR') != NULL) ? request()->server('SERVER_ADDR') : request()->getClientIp();
		// $user->last_browser = \App\Utility\Common::get_browser_name(request()->server('HTTP_USER_AGENT'));
		// $user->logged_in = true;
		// $user->save();
	}

	/**
	 * Handle user logout events.
	 */
	public function onUserLogout($event) {
		// $user = $event->user;
		// if (!empty($user)) {
		// 	$user->logged_in = false;
		// 	$user->save();
		// }
	}

	/**
	 * Register the listeners for the subscriber.
	 *
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events) {
		$events->listen(
			'Illuminate\Auth\Events\Login',
			'App\Listeners\UserEventSubscriber@onUserLogin'
		);

		$events->listen(
			'Illuminate\Auth\Events\Logout',
			'App\Listeners\UserEventSubscriber@onUserLogout'
		);
	}
}
