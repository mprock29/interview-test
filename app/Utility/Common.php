<?php

namespace App\Utility;

use App\Models\EmailLogs;
use App\Models\Logs;
use App\Models\Menus;
use App\Models\RolePermissions;
use App\Models\SmsLogs;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;

class Common
{

    
    public static function getYesNo($id = null)
    {
        $data = [0 => 'No', 1 => 'Yes'];
        return isset($id) ? $data[$id] : $data;
    }

    public static function getStatus($id = null)
    {
        $data = [0 => 'Inactive', 1 => 'Active'];
        return isset($id) ? $data[$id] : $data;
    }

    public static function getMode($id = null)
    {
        $data = [0 => 'Dark Mode', 1 => 'Light Mode'];
        return isset($id) ? array_key_exists($id, $data) ? $data[$id] : $id : $data;
    }

    public static function getFileExtension($id = null)
    {
        $data = ['pdf' => 'pdf', 'excel' => 'xlsx', 'csv' => 'csv'];
        return isset($id) ? array_key_exists($id, $data) ? $data[$id] : $id : $data;
    }

   

    public static function checkActionAccess($pageUrl)
    {
        $status = 0;
        $userId = \Auth::user()->id;
        $userRoleId = \Auth::user()->role_id;
        if ($userRoleId == 1 && $userId == config('app.super_admin_id')) {
            return true;
        } else {
            $menuID = Menus::where('page_url', $pageUrl)->where('deleted_at', null)->first();
            if (isset($menuID['id']) && !empty($menuID['id'])) {
                $status = RolePermissions::where('role_id', $userRoleId)->where('menu_id', $menuID['id'])->count();
            }
            return ($status != 0) ? true : false;
        }
    }

    public static function sideBarDesign()
    {
        /* SELECTED CURRENT URL */
        $currentRoutes = \Request::route()->getName();
        $userId = \Auth::user()->id;
        $userRoleId = \Auth::user()->role_id;
        $selected = array();
        $currentMenu = Menus::select('menus.*')->where('menus.page_url', $currentRoutes)->orderBy("sort_order", "ASC")->first();
        $currentPage = !empty($currentMenu->id) ? $currentMenu->id : 0;

        $selected[] = $currentPage;
        /* CURRENT RIGHTS FOR PASSED USER GROUP */
        $modelRights = RolePermissions::where('role_id', $userRoleId)->get();
        $permissionsArr = array();
        if (!empty($modelRights)):
            foreach ($modelRights as $value):
                $permissionsArr[] = $value->menu_id;
            endforeach;
        endif;
        /* GET MENU ONLY SHOW IN */
        if ($userRoleId == config('app.super_admin_role_id') && $userId == config('app.super_admin_id')) {
            $model = Menus::select('menus.*')->where('menus.deleted_at', null)->orderBy("sort_order", "ASC")->get();
        } else {
            $model = Menus::select('menus.*')->where('menus.deleted_at', null)->whereIn('id', $permissionsArr)->orderBy("sort_order", "ASC")->get();
        }
        $arr = array();
        if (!empty($model)):
            foreach ($model as $value):
                if ($value->show_in_menu):
                    $active = ($value->id == $currentPage) ? true : false;
                    $arr[] = array(
                        "id" => $value->id,
                        "parent_id" => $value->parent_id,
                        "child_menu_id" => $value->child_menu_id,
                        "menu_title" => $value->menu_title,
                        "menu_icon" => $value->menu_icon,
                        "page_url" => $value->page_url,
                        "active" => $active,
                    );
                endif;
                if ($value->id == $currentPage):
                    $selected[] = $value->id;
                    $selected[] = $value->parent_id;
                    $selected[] = $value->child_menu_id;
                    $currentPage = $value->parent_id;
                endif;
            endforeach;
        endif;
        $menusArr = self::buildMenuTree($arr, null, 0, $selected);
        return $menusArr;
    }

    public static function buildMenuTree($ar, $pid = null, $level = 0, $selected = array())
    {
        $levelClass = array(1 => "nav nav-second-level", 2 => 'nav nav-third-level');
        $op = array();
        foreach ($ar as $item) {
            if ($item['parent_id'] == $pid) {
                $level = empty($pid) ? 0 : $level;
                $selected[] = $item['parent_id'];
                $selected[] = $item['child_menu_id'];
                $op[$item['id']] = array(
                    'id' => $item['id'],
                    'parent_id' => $item['parent_id'],
                    'child_menu_id' => $item['child_menu_id'],
                    'menu_title' => $item['menu_title'],
                    'url' => $item['page_url'],
                    'menu_icon' => $item['menu_icon'],
                    'active' => in_array($item["id"], $selected) ? true : false,
                );
                // using recursion function php
                $children = self::buildMenuTree($ar, $item['id'], $level, $selected);
                if ($children) {
                    $level++;
                    $op[$item['id']]['level_class'] = !empty($levelClass[$level]) ? $levelClass[$level] : "";
                    $op[$item['id']]['children'] = $children;
                }
            }
        }
        return $op;
    }

    public static function setDateTimeFormat($date_time, $format = 'd/m/Y H:i')
    {
        $date_time = str_replace("/", "-", $date_time);
        $converted_date_time = self::convert_timezone($date_time, 'Asia/Kolkata', 'UTC');
        return date($format, strtotime($converted_date_time));
    }

    public static function setLocalDateTimeFormat($date_time, $format = 'd/m/Y H:i')
    {
        $date_time = str_replace("/", "-", $date_time);
        $converted_date_time = date($format, strtotime('+5 hour +30 minutes', strtotime($date_time)));
        return $converted_date_time;
    }

    public static function convert_timezone($date = "", $to_timezone = '', $from_timezone = '', $format = 'Y-m-d H:i:s')
    {
        // date_default_timezone_set($from_timezone);
        $new_date_time = strtotime($date);
        // date_default_timezone_set($to_timezone);
        $converted_date_time = date($format, $new_date_time);
        // date_default_timezone_set('UTC');
        // dd($converted_date_time);
        return $converted_date_time;
    }

    public static function convertDateTimeToUtc($date_time)
    {
        $date_time = str_replace("/", "-", $date_time);
        $converted_date_time = self::convert_timezone($date_time, 'UTC', 'Asia/Kolkata');
        $utc_timestamp = strtotime($converted_date_time);
        return $utc_timestamp;
    }

    public static function getCurrentTimeStamp()
    {
        return strtotime(date('Y-m-d H:i:s'));
    }

    

    public static function getOwnerships($id = null)
    {
        $data = [
            '0' => 'Individual',
            '1' => 'Company',
            '2' => 'Govt.',
        ];
        return isset($id) ? $data[$id] : $data;
    }

    public static function restrictedUrl()
    {
        \Session::flash('restricted', 'Restricted Data Access Not Allowed.');
    }

    public static function getGender($id = null)
    {
        $data = [
            '0' => 'Male',
            '1' => 'Female',
            '2' => 'Other',
        ];
        return isset($id) ? $data[$id] : $data;
    }

    public static function getMaritalStatus($id = null)
    {
        $data = [
            '0' => 'Married',
            '1' => 'UnMarried',
        ];
        return isset($id) ? $data[$id] : $data;
    }

    public static function getDisability($id = null)
    {
        $data = [
            '0' => 'No',
            '1' => 'Yes',
        ];
        return isset($id) ? $data[$id] : $data;
    }

    public static function getTimeDifference($start_time, $end_time)
    {
        $startTime = Carbon::parse($start_time);
        $endTime = Carbon::parse($end_time);
        $totalTime = $endTime->diffForHumans($startTime);
        return $totalTime;
    }

    
    public static function getAttributeName($field = null)
    {
        $attributes = \Lang::get('validation.attributes');
        if (isset($attributes[$field])) {
            return $attributes[$field];
        } else {
            return $field;
        }
    }

    public static function resJson($data)
    {
        $response = [
            'success' => true,
            'total' => $data,
        ];
        return response()->json($response);
    }

    public static function acceptedRouteArray()
    {
        return [
            'getstateName',
            'get_child_menu',
            'dashboard.getData',
            'dashboard.orderList',
            'user.download-file.get',
            'user.download-file.post',
        ];
    }

    public static function addEditUtcDateTime($format = 'Y-m-d H:i:s', $count = 0, $raw = 'days', $process = '+')
    {
        $date = date('Y-m-d H:i:s');
        return date($format, strtotime($process . $count . ' ' . $raw, strtotime($date)));
    }

    public static function getCurrentUTCDateTime($format = 'Y-m-d H:i:s')
    {
        return date($format);
    }

   

}
