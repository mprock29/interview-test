<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Models\User;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('auth.change_password');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
         {
            $request->validate([
                'current_password' => ['required', new MatchOldPassword],
                'new_password' => ['required','max:10'],
                'new_confirm_password' => ['required','same:new_password','max:10'],
            ]);
            User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
            Session::flash('success', 'Password Change Succesfully.');             
            return view('auth.change_password');            
        }
    }
}
