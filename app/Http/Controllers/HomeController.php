<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Orders;
use App\Utility\Common;
use File;
use \Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Mail;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		$user_data = User::where('role_id',2)->get();
		return view('home', [
			'devices' => [],
			'usersLoginHistory' => [],
			'user_data' => $user_data,
		]);
	}

	

	public function edit(Request $request)
	{
		$result = \DB::transaction(function () use ($request) {
			try {
				if ($request->post()) {
					$input = $request->all();
					$user = \Auth::user();
					$validate = $user->validate_users_profile($input);
					if ($validate->fails()) {
						return redirect()->back()->withErrors($validate)->withInput();
					} else {
						$file = request()->file('profile_pic');
						if ($file) {
							$imageName = preg_replace('/[^a-zA-Z0-9_.]/', '', time() . '_' . $file->getClientOriginalName());
							$file->move(public_path('/uploads/users/'), $imageName);
							$input['profile_pic'] = $imageName;
							$old_file = public_path('uploads/users') . '/' . $user->profile_pic; // if your file is in some folder in public directory.
							$file_path = public_path('uploads/users') . '/' . $request->profile_pic; // if your file is in some folder in public directory.
							if (\File::exists($file_path)) {
								\File::delete($file_path); //for deleting only file try this
							}
							if (\File::exists($old_file)) {
								\File::delete($old_file); //for deleting Old file
							}
						}
						$user->update($input);
						Session::flash('success', "User Details Updated Successfully.");
						return redirect()->route('user.edit');
					}
				}
				$maritalStatus = Common::getMaritalStatus();
				$viewMode = Common::getMode();
				return view('auth.update_profile', ['maritalStatus' => $maritalStatus, 'viewMode' => $viewMode]);
			} catch (Exception $ex) {
				throw new Exception($ex);
			}
		});
		return $result;
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function restrictedUrl()
	{
		return view('errors.403');
	}

	public function downloadFile($file_path, Request $request)
	{
		if (isset($file_path)) {
			// $enc =  \Crypt::encrypt('uploads/report_files/horn_duration_count_2021_10_07_10_33_AM/1_horn_duration_count_2021_10_07_10_33_AM.xlsx');
			if ($request->post()) {
				try {
					$decrypted = \Crypt::decrypt($file_path);
				} catch (\Exception $ex) {
					// Session::flash('error', "File is removed. Please request new file.");
					// return view('users.download', ['filePath' => $file_path]);
					return back()->with('error', 'File is removed. Please request new file.');
				}
				if (File::exists($decrypted)) {
					Session::flash('success', "File will download soon.");
					return response()->download($decrypted);
				} else {
					// Session::flash('error', "File is removed. Please request new file.");
					// return view('users.download', ['filePath' => $file_path]);
					return back()->with('error', 'File is removed. Please request new file.');
				}
			}
			return view('users.download', ['filePath' => $file_path]);
		} else {
			abort(404);
		}
	}

	
}
