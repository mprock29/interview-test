<?php

namespace App\Http\Controllers;

use App\Models\Roles;
use App\Models\User;
use App\Utility\Common;
use App\Utility\SMS;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Mail;
use Illuminate\Support\Facades\Auth;
use \Carbon\Carbon;

class FrontUsersController extends Controller
{
	public function __construct()
	{
		// $this->middleware('auth');
	}

	public function frontUser()
	{
		return view('front.home');
	}
	public function frontUserRegister()
	{
		return view('front.register');
	}
	public function frontUserLogin()
	{
		return view('front.login');
	}
	public function dashboard()
	{
		
		// if();
		if(Auth::user()->role_id == 2){

			$current_user_id = Auth::id();
			$current_user = User::where('id',$current_user_id)->first();
			$user_data = User::where('role_id',2)->where('gender','<>',$current_user->gender)->orderBy('id','DESC')->get()->toArray();
		}else{

			return redirect()->route('home');
		}

		// $filter_data = array_map(function($user) use ($current_user){
			// if($current_user->gender != $user['gender']){
				// dd($user);
			// }
		// },$user_data);
		// dd($user_data);

		


		$filter_data = array_map(function($user) use ($current_user){
			// dd();
			$user['percentage'] = 0;
			if($user != ""){

				$partner_expected_income = explode(';',$current_user->partner_expected_income);			
				if($user['annual_income'] >= $partner_expected_income[0] && $user['annual_income'] <= $partner_expected_income[1]){
					$user['percentage'] = 25;

					return $user;
				}
			}
			
		},$user_data);
		
		
		$filter_data = array_map(function($user) use ($current_user){
			// dd();
			
			if($user != ""){
				
				// dd($current_user->partner_manglik);
				if($current_user->partner_manglik =="Both" ){
					$user['percentage'] = $user['percentage']+25;
					
					return $user;
				}else if ($user['manglik'] == $current_user->partner_manglik){
					// dd($user);
					$user['percentage'] = $user['percentage']+25;

					return $user;

				}else{
					return $user;

				}
			}
			
		},$filter_data);
		$filter_data = array_map(function($user) use ($current_user){
			// dd();
			if($user != ""){

				$occupation = explode(',',$current_user->partner_occupation)			;

				if(in_array($user['occupation'],$occupation)){
					$user['percentage'] = $user['percentage']+25;
					return $user;
				}else{
					return $user;

				}
			}
			
		},$filter_data);
		$filter_data = array_map(function($user) use ($current_user){
			// dd();
			if($user != ""){

				$occupation = explode(',',$current_user->partner_family_type)			;

				if(in_array($user['family_type'],$occupation)){
					$user['percentage'] = $user['percentage']+25;
					return $user;
				}else{
					return $user;

				}
			}
			
		},$filter_data);
		
		// dd($filter_data);
		return view('front.dashboard',['user_data'=>$filter_data]);
	}


	public function frontUserLoginSubmit(Request $request)
	{
		// dd($request->all());
		$validated = $request->validate([
			'email' => 'required|email',
			'password' => 'required',
		]);
		
		$input = $request->all();
		
		
		if (Auth::attempt(['email' => $request->email, 'password' => $request->password,'role_id'=>2])) {

			return redirect()->route('front.dashboard');
		}else{			
			return redirect()->route('front.login');
		}

	}



	public function frontUserRegisterSubmit(Request $request)
	{
		// dd($request->all());
		$validated = $request->validate([
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email|unique:users',
			'password' => 'required',
			'date_of_birth' => 'required',
			'annual_income' => 'required',
			'occupation' => 'required',
			'family_type' => 'required',
			'manglik' => 'required',
			'gender' => 'required',
			'partner_expected_income'=> 'required',
			'partner_occupation'=> 'required',
			'partner_family_type'=> 'required',
			'partner_manglik'=> 'required',
		]);
		
		$input = $request->all();
		$input['role_id'] = 2;
		$input['partner_occupation'] = implode(',',$input['partner_occupation']);
		$input['partner_family_type'] = implode(',',$input['partner_family_type']);
		$input['password'] = bcrypt($input['password']);
		$users = User::create($input);
		Session::flash('success', "Registered");
		return redirect()->route('front.login');

	}
}
