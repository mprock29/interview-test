<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Roles;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

    }

    public function login(Request $request)
    {
        // $this->redirectTo = $request->session()->has('url.intended') ? $request->session()->get('url.intended') : RouteServiceProvider::HOME;
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $user = User::where('email', $request->email)->first();
        $remember_me = $request->has('remember_token') ? true : false;
        $customRestrict = 0;
        if (!$user) {
            $customRestrict = 1;
            $errormessage = trans('auth.failed');
            $errors = [$this->username() => $errormessage];
        }
        // if ($customRestrict == 0 && $user->status != config('app.status.active')) {

        //     $customRestrict = 1;
        //     $errormessage = trans('auth.suspended-account');
        //     $errors = [$this->username() => $errormessage];
        // }
        // if ($customRestrict == 0 && (Roles::where('id', $user->role_id)->where('status', '<>', config('app.status.active'))->exists())) {
        //     $customRestrict = 1;

        //     $errormessage = trans('auth.suspended-account');
        //     $errors = [$this->username() => $errormessage];
        // }
        if (($user->role_id ==2)) {
            $customRestrict = 1;

            $errormessage = trans('auth.suspended-account');
            $errors = [$this->username() => $errormessage];
        }
        // dd(Roles::where('id', $user->role_id)->where('status', '<>', config('app.status.active'))->exists());
        // dd($customRestrict, config('app.status.active'), Roles::where('id', $user->role_id)->where('status', '<>', config('app.status.active'))->exists());

        //set cookie for remember me
        if (!empty($remember_me)) {
            Cookie::queue('email', $request->email, time() + (10 * 365 * 24 * 60 * 60));
            Cookie::queue('password', $request->password, time() + (10 * 365 * 24 * 60 * 60));
        } else {
            Cookie::queue(Cookie::forget('email'));
            Cookie::queue(Cookie::forget('password'));
        }
        if ($customRestrict == 1) {
            Session::flash('error', $errormessage);
            return redirect()->back()
                ->withInput($request->only($this->username(), 'remember'))
                ->withErrors($errors);
        } else {
            // dd('afafafaf');
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember_me)) {

                // dd($this->redirectTo);
                // Auth::logoutOtherDevices($request->password);
                return redirect($this->redirectTo);
            }else{
                // return view('errors.403');
                return $this->sendFailedLoginResponse($request);
            }
        }
        return $this->sendFailedLoginResponse($request);
    }

}
