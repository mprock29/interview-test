<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Utility\Common;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use App\Models\RolePermissions;
use App\Models\Menus;
use App\Models\Groups;
use App\Models\Roles;


class UserAuthenticateWithRoles
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
            $userRoleId = \Auth::user()->role_id;
            $accepted = \Common::acceptedRouteArray();
                if(in_array(\Request::route()->getName(),$accepted)) {
                    return $next($request);
                }
                if ($userRoleId != config('app.super_admin_id')) {
                    // dd(\Auth::user()->roles);
                    if(!\Auth::user() || \Auth::user()->status != config('app.status.active')) {
                        $request->session()->invalidate();
                        $request->session()->regenerateToken();
                        return redirect()->route('login')->with('error', trans('auth.suspended-account'));
                    }
                    if(!\Auth::user()->roles || Roles::where('id', \Auth::user()->role_id)->where('status', '<>', config('app.status.active'))->exists()) {
                        $request->session()->invalidate();
                        $request->session()->regenerateToken();
                        return redirect()->route('login')->with('error', trans('auth.suspended-account'));
                    }      
                    if(\Auth::user()->role_id == 2 || \Auth::user()->role_id == 3 || \Auth::user()->role_id == 4) {
                        $request->session()->invalidate();
                        $request->session()->regenerateToken();
                        return redirect()->route('login')->with('error', trans('auth.suspended-account'));
                    }                  
                    $status = '';
                    $data = \Request::route()->getName();
                    $menuID = Menus::where('page_url', $data)->where('deleted_at', NULL)->first();

                    if (isset($menuID['id']) && !empty($menuID['id'])) {
                        $status = RolePermissions::where('role_id', $userRoleId)->where('menu_id', $menuID['id'])->count();
                    }

                    if ($status != 1) {
                        if($data == 'home') {
                            $permitedRoute = RolePermissions::where('role_id', $userRoleId)->first();
                            if(!isset($permitedRoute)) {
                                Auth::logout();
                                \Session::flash('error', 'You don\'t have any permission. Please ask your superviser to assign you permission.');
                                return redirect('login');
                            } else {
                                return redirect($permitedRoute->page_url);
                            }
                        }
                        Common::restrictedUrl();
                        return redirect('home');
                    }
                }
        return $next($request);
    }
}
