<?php

namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Spatie\Activitylog\Traits\LogsActivity;

class Menus extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Sortable;
    // use LogsActivity;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;

    public $table = 'menus';
    protected $fillable = [
        'id',
        'parent_id',
        'child_menu_id',
        'menu_title',
        'page_url',
        'show_in_menu',
        'sort_order',
        'menu_icon',
        'status',
        'created_by',
        'updated_by',
    ];

    private $rules = [
        'menu_title' => 'required',
        'page_url' => 'required',
        'menu_icon' => 'required',
        'status' => 'required',
    ];
    private $messages = [
        'menu_title.required' => 'Menu title is required.',
        'page_url.required' => 'Page url is required.',
        'menu_icon.required' => 'Menu icon is required.',
        'Status.required' => 'Status is required.',
    ];


    // protected static $logAttributes = [
    //     'id',
    //     'parent_id',
    //     'child_menu_id',
    //     'menu_title',
    //     'page_url',
    //     'show_in_menu',
    //     'sort_order',
    //     'menu_icon',
    //     'status',
    //     'created_by',
    //     'updated_by',
    // ];
    // protected static $logOnlyDirty = true; //only update input display property column



    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = \Auth::user()->id;
            $model->updated_by = \Auth::user()->id;
        });

        static::updating(function ($model) {
            $model->updated_by = \Auth::user()->id;
        });
    }

    public function scopeStatus($query)
    {
        return $query->where('status', config('app.status.active'));
    }

    public function validate_menus($data, $id = null)
    {
        return Validator::make($data, $this->rules, $this->messages);
    }

    public function parentMenu()
    {
        return $this->belongsTo(Menus::class, 'parent_id');
    }

    public function parentMenuList()
    {
        return $this->hasMany(self::class, 'parent_id', 'id')->where('child_menu_id', 0);
    }
    public function childMenuList()
    {
        return $this->hasMany(self::class, 'child_menu_id', 'id');
    }

    public static function getParentMenuList()
    {
        $menu = parent::where('parent_id', config('app.no_yes.no'))->orderBy("sort_order", "ASC")->get();
        $menuList = [];

        if ($menu) {
            foreach ($menu as $key => $value) {
                $menuList[$value->id] = $value->menu_title;
            }
        }
        return $menuList;
    }

    public static function getChildMenuList($parent_id)
    {        
        $menu = parent::where('parent_id', $parent_id)->where('child_menu_id', 0)->orderBy("sort_order", "ASC")->get();
        $menuList[''] = trans('menus.lbl_select_child_menu');

        if ($menu) {
            foreach ($menu as $value) {
                $menuList[$value->id] = $value->menu_title;
            }
        }
        return $menuList;
    }
}
