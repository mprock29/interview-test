<?php

namespace App\Models;

use App\Utility\SMS;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Kyslik\ColumnSortable\Sortable;
use Laravel\Passport\HasApiTokens;
use Mail;
use \Carbon\Carbon;
use DateTimeInterface;
class User extends Authenticatable
{
	use HasFactory, Notifiable;
	use SoftDeletes;
	use Sortable;
	use HasApiTokens;

	public $table = 'users';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_DELETED = 2;
	const FRONT_USER = 2;

	const downloadType = [
		'excel' => 'Excel',
		'csv' => 'CSV',
		'pdf' => 'PDF',
	];
	protected $fillable = [
		'role_id',
		'first_name',
		'last_name',
		'email',
		'password',
		'date_of_birth',
		'annual_income',
		'occupation',
		'family_type',
		'manglik',
		'gender',
		'partner_expected_income',
		'partner_occupation',
		'partner_family_type',
		'partner_manglik',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	

	

	
	public function scopeRestricted($query)
	{
		if (\Auth::user()->role_id == 1) {
			return $query;
		}
		return $query->whereHas('roles', function ($q) {
			$q->whereIn('groups_id', explode(',', \Auth::user()->roles->groups_id));
		});
	}
}
