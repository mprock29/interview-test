<?php

namespace App\Models;

use App\Utility\Common;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class BaseModel extends Model {

    public $no_image = '/img/no_image.png';
    public static $default_login_page_image = '/img/default_bg.jpg';

    /**
     * Validate Post Request
     * @param $data
     * @return mixed
     */
    public function validate($data, $rules = [], $messages = []) {
        $validator = Validator::make($data, $rules, $messages);
        return $validator;
    }

    /**
     * Scope a query to only include domain specific records.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatus($query) {
        return $query->where('status', config('app.status.enable'));
    }

    /**
     * Usage: Validate dimension
     * @param $file_path
     * @param $width
     * @param $height
     * @return bool
     */
    public function validate_image_dimension($file_path, $width, $height) {
        list($actual_width, $actual_height) = @getimagesize(public_path($file_path));

        if (($actual_width >= $width) && ($actual_height >= $height)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Usage: Upoad images
     * @param $source
     * @param $destination
     * @return mixed
     */
    public function save_image($source, $destination, $old_image = null) {
        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true, true);
        }

        if (is_object($source)) {
            $file_name = substr(md5(uniqid(rand(), true)), 0, 8) . '.' . $source->getClientOriginalExtension();
        } else {
            $pathinfo = pathinfo(public_path($source));
            $file_name = time() . '.' . $pathinfo['extension'];
            $source = public_path($source);
        }


        if (strpos($source, 'uploads')) {
            return $pathinfo['filename'] . '.' . $pathinfo['extension'];
        } else {
            if (File::copy($source, public_path($destination . DIRECTORY_SEPARATOR . $file_name))) {
                if (!empty($old_image) && file_exists(public_path($destination . DIRECTORY_SEPARATOR . $old_image))) {
                    unlink(public_path($destination . DIRECTORY_SEPARATOR . $old_image));
                }
                return $file_name;
            } else {
                return false;
            }
        }
    }

    /**
     * Usage: Delete image
     * @param $image
     * @param $path
     */
    public function delete_image($image, $path) {
        if (!empty($image) && file_exists($path . DIRECTORY_SEPARATOR . $image)) {
            unlink(public_path($path . DIRECTORY_SEPARATOR . $image));
        }
    }

    /**
     * Usage: Get Assets URLs
     * @param $directory
     * @param $name
     * @return string
     */
    public function get_absolute_url($directory, $name) {

        return asset(DIRECTORY_SEPARATOR . $directory) . DIRECTORY_SEPARATOR . $name;
    }

    /**
     * Usage: Get Assets URLs
     * @param $directory
     * @param $name
     * @return string
     */
    public function get_relative_url($directory, $name) {

        return public_path(DIRECTORY_SEPARATOR . $directory) . DIRECTORY_SEPARATOR . $name;
    }

    /**
     * Usage: Get bootstrap Label Badge
     * @param $array
     * @param $class
     * @return string
     */
    public function getLabels($array, $class) {
        $str = '';
        if (!empty($array)) {
            $i = 0;
            foreach ($array as $key => $value) {
                if ($i == 0)
                    $str = '<span class="' . $class . '">' . $value . '</span>';
                else
                    $str .= '<br /><span class="' . $class . '">' . $value . '</span>';

                $i++;
            }
        }

        return $str;
    }

    /**
     * Usage: Get bootstrap Label Links
     * @param $array
     * @param $class
     * @return string
     */
    public function getLinks($array, $class) {
        $str = '';
        if (!empty($array)) {
            $i = 0;
            foreach ($array as $key => $value) {
                if ($i == 0)
                    $str = '<span class="' . $class . '"><a href="' . $value . '" target="_blank">' . $value . '</a></span>';
                else
                    $str .= '<br /><span class="' . $class . '"><a href="' . $value . '" target="_blank">' . $value . '</a></span>';

                $i++;
            }
        }

        return $str;
    }

    /**
     * Usage: Get SQL
     *
     * @param $builder
     * @return mixed
     */
    public static function getSQL($builder) {
        $sql = $builder->toSql();
        foreach ($builder->getBindings() as $binding) {
            $value = is_numeric($binding) ? $binding : "'" . $binding . "'";
            $sql = preg_replace('/\?/', $value, $sql, 1);
        }
        return $sql;
    }

    /**
     * Usage: Check URL is valid or not
     */
    public function isValidURL($url) {
        if (!empty($url) && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) {
            return false;
        }
        return true;
    }
    
    /**
     * Usage: Reorder List
     * @param $table
     * @param $data
     */
    public function reorder_list($table, $data, $master_role_id)
    {
        $update_query = "UPDATE storx_{$table} SET sort_order = CASE";
        foreach ($data as $index => $id) {
            $update_query .= ' WHEN id = ' . $id . ' THEN ' . ($index + 1);
        }
        $update_query .= ' ELSE null END WHERE  master_role_id = '.$master_role_id.' AND ID IN (' . implode(',', $data) . ')';
        DB::update("UPDATE `storx_{$table}` SET sort_order = ".config('app.status.disable')." WHERE master_role_id = '.$master_role_id.'  AND status = ".config('app.status.disable'));
        return DB::statement($update_query);
    }
}
