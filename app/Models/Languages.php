<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;
class Languages extends Model
{
    use HasFactory;
    public $table = 'languages';
    
    protected $fillable = ['title', 'lang_code', 'default_lang', 'lang_flag', 'status'];
    
    public static function getLanguagesList()
    {
        $langItems = self::select('languages.*')->where('languages.status', 1)->get();
        if ($langItems) {
            foreach ($langItems as $k => $v) {
                $langList[$v->id] = $v->title;
            }
        }        
        return $langList;
    }
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
