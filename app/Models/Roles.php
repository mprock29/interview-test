<?php

namespace App\Models;

use App\Models\User;
use App\Scopes\ActiveScope;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Kyslik\ColumnSortable\Sortable;

class Roles extends Model {
	use HasFactory;
	use SoftDeletes;
	use Sortable;

	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	const STATUS_DELETED = 2;

	public $table = 'roles';
	protected $fillable = ['id', 'name', 'status', 'created_by', 'updated_by'];

	public function users() {
		return $this->hasMany(User::class, 'role_id');
	}

	private $rules = [
		'name' => 'required|unique:roles,name,NULL,id,deleted_at,NULL',
		'status' => 'required',
	];
	private $messages = [
		'name.required' => 'Role name is required.',
		'name.unique' => 'Role name is already exist.',
		'Status.required' => 'Status is required.',
	];

	/*created_by and updated by function for automatically created and updated user id */
	public static function boot() {
		parent::boot();

		static::addGlobalScope(new ActiveScope);

		static::creating(function ($model) {
			$model->created_by = \Auth::user()->id;
			$model->updated_by = \Auth::user()->id;
		});

		static::updating(function ($model) {
			$model->updated_by = \Auth::user()->id;
		});
	}

	public function validate_roles($data, $id = null) {
		if (!empty($id)) {
			$this->rules['name'] = ['required', \Illuminate\Validation\Rule::unique('roles', 'name')->whereNotIn('id', [$id])->where('deleted_at', NULL)];
		}
		return Validator::make($data, $this->rules, $this->messages);
	}

	//Get the user table Relation
	public static function userRelation($id = null) {
		return User::where('id', $id)->first();
	}
}