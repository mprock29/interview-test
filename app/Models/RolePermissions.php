<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Roles;

class RolePermissions extends Model
{
    use HasFactory;

    public $table = 'roles_permissions';

    public $fillable = ['role_id', 'menu_id', 'status', 'created_at', 'updated_at'];

    private $rules = [
        'role_id' => 'required',
        'menu_id' => 'required'
    ];
    private $messages = [];

    public $description_limit = [
        'max' => 1000
    ];

    /*created_by and updated by function for automatically created and updated user id */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = \Auth::user()->id;
        });

        static::updating(function ($model) {
            $model->updated_by = \Auth::user()->id;
        });
    }

    public function getRolePermissionList($role_id, $select)
    {    
        $RolePermissionCollection = [];
        if (!empty($role_id)) {
            $role = Roles::where('id', $role_id)->first();
            if (!empty($role)) {
                $RolePermission = self::join('menus', 'roles_permissions.menu_id', '=', 'menus.id')
                    ->select($select)
                    ->where([
                        'roles_permissions.role_id' => $role_id,
                        'menus.status' => config('app.status.active'),
                        'menus.deleted_at' => NULL,
                        'roles_permissions.status' => config('app.status.active')
                    ])->get()->toArray();                
                if (count($RolePermission) > 0) {
                    foreach ($RolePermission as $key => $value) {
                        $RolePermissionCollection[] = isset($value['id']) ? $value['id'] : strtolower($value['name']);
                    }
                }
            }
        }        
        return $RolePermissionCollection;
    }
}
