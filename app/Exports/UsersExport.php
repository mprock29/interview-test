<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithProperties;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\FromArray;
use App\Utility\Common;

class UsersExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithStyles, WithEvents
{
    private $input;
    private $data;
    private $headings;
   
    public function __construct($input, $data)
    {
        $this->input = $input;
        $this->data = $data;
        $this->headings = array_values(Common::generateExportUsersHeaders());    
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            'A1'    => ['font' => ['bold' => true]],
            1       => ['font' => ['bold' => true]],
        ];
    }

    public function headings() : array
    {
        return $this->headings;
    }

    public function map($data): array
    {
        return array_values(Common::generateExportUserData($data));
    }

    public function columnWidths(): array
    {
        return [
            // 'A' => 55,
        ];
    }

    public function collection()
    {
        return $this->data;
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->insertNewRowBefore(1); 
                $event->sheet->setCellValue('A1', trans('users.lbl_admin_user_list') );
                $event->sheet->mergeCells('A1:B1');
                $event->sheet->getStyle('A1')->applyFromArray(['font' => ['bold' => true]]);
            },
        ];
    }
}
