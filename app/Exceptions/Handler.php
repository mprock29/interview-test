<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Arr;
use Log;
use Symfony\Component\ErrorHandler\ErrorRenderer\HtmlErrorRenderer;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Throwable;

class Handler extends ExceptionHandler {
	/**
	 * A list of the exception types that are not reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
	];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array
	 */
	protected $dontFlash = [
		'current_password',
		'password',
		'password_confirmation',
	];

	/**
	 * Report or log an exception.
	 *
	 * @param  \Throwable  $exception
	 * @return void
	 *
	 * @throws \Exception
	 */
	public function report(Throwable $exception) {
		// if ($this->shouldReport($exception)) {
		//     $this->sendEmail($exception); // sends an email
		// }
		if ($exception instanceof \League\OAuth2\Server\Exception\OAuthServerException && $exception->getCode() == 9) {
			return;
		}
		if (config('app.env') == 'production' || config('app.debug') === false) {
			if ($this->shouldReport($exception)) {
				$this->sendEmail($exception);
			}
		}

		parent::report($exception);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Throwable  $exception
	 * @return \Symfony\Component\HttpFoundation\Response
	 *
	 * @throws \Throwable
	 */
	public function render($request, Throwable $exception) {

		// dd($exception,$exception->guards());
		if ($exception->getMessage() == "Unauthenticated."&& $exception->guards()[0] == "api" ) {

			$json = [
				'success' => false,
				'message' => $exception->getMessage(),
			];
			return response()
				->json($json, 401);

		}
		if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
			//https://gist.github.com/jrmadsen67/bd0f9ad0ef1ed6bb594e
			return redirect()
				->back()
				->withInput($request->except('password'))
				->with('errorMessage', 'This form has expired due to inactivity. Please try again.');
		}
		if ($this->isHttpException($exception)) {
			if ($exception->getStatusCode() == 404) {
				return response()->view('errors.404', [], 404);
			}
			if ($exception->getStatusCode() == 500) {
				return response()->view('errors.500', [], 500);
			}
			if ($exception->getStatusCode() == 403) {
				return response()->view('errors.403', [], 403);
			}
		}

		return parent::render($request, $exception);
	}

	/**
	 * Sends an email to the developer about the exception.
	 *
	 * @return void
	 */
	public function sendEmail(Throwable $exception) {
		try {
			$e = FlattenException::create($exception);
			$handler = new HtmlErrorRenderer(true); // boolean, true raises debug flag...
			$content = $handler->getBody($e);

			\Mail::send('emails.exception', compact('content'), function ($message) {
				$message
					->to(['mithil.panchal@drcsystems.com'])
					// ->cc(['nikunj.rathod@drcsystems.com', 'bhargav.katakpara@drcsystems.com'])
					->cc(['bhargav.katakpara@drcsystems.com'])
					->subject('Exception: ' . config('app.name') . ' - ' . Date('Y-m-d h:i:sa'))
				;
			});
		} catch (Throwable $ex) {
			Log::error($ex);
		}
	}
}
