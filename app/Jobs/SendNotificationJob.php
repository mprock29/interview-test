<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\NotificationMaster;
use App\Models\User;

use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Exception\Messaging\InvalidMessage;


class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $send_to;
    public $device_type;
    public $notificationData;
    public $tries = 1;
    public $subject;
    public $content;
    public $custom_attribute;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($notificationData)
    {   
        $this->subject = $notificationData->title;
        $this->content = $notificationData->message;
        $this->notificationData = $notificationData;
        $this->custom_attribute = json_decode($notificationData->custom_attribute);
        $this->send_to = $notificationData->userData->device_token;
        $this->device_type = $notificationData->userData->device_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::channel("send-notification-job")->info("SendNotificationJob::START");
        \Log::channel("send-notification-job")->info("SendNotificationJob::NotificationID - " . $this->notificationData->id);
        \Log::channel("send-notification-job")->info($this->send_to);
        if (isset($this->send_to) && $this->send_to != '') {
            // $device_token = 'elt0ZMVOpEZBkeBtQuhGPj:APA91bGjDhCbcQLR88FopVZpDWJWUptUCLY9KRcqDKsYW5lRkJB1MqHYNBZ9E-bY3HL2iHHU8gcbhFwtaGC68dJcaV_XxRihH0LYYEkM7rYBrkiEiUR0mE2_JUFogZhdAP_vXCV6KlYv';
            $device_token = $this->send_to;
            $device_type = $this->device_type;
            $title = $this->subject;
            $body = $this->content;
            $custom_attribute = $this->custom_attribute;

            try {
                $SERVER_API_KEY = FCM_SERVER_KEY;
                if (isset($device_type) && $device_type == 'ios') {
                    $notification_data = [
                        "to" => $device_token,
                        "content_available" => true,
                        "mutable_content" => true,
                    ];

                    $notification_data['data'] = $custom_attribute['data'];
                    $notification_data['notification'] = $custom_attribute['notification'];

                } else if (isset($device_type) && $device_type == 'android') {
                    $notification_data = [
                        "to" => $device_token,
                        "content_available" => true,
                        "mutable_content" => true,
                    ];

                    $notification_data['data'] = $custom_attribute['data'];
                    $notification_data['notification'] = $custom_attribute['notification'];
                }

                $data_string = json_encode($notification_data);
                $headers = [
                    'Authorization: key=' . $SERVER_API_KEY,
                    'Content-Type: application/json',
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

                $response = curl_exec($ch);
                $response = json_decode($response);
                if (isset($response->success) && $response->success == 1) {
                    NotificationMaster::notificationUpdateStatus($this->notificationData->id, NotificationMaster::IS_SENT_YES);
                }
            } catch (InvalidMessage $e) {
                \Log::channel("send-notification-job")->info('SendNotificationJob::InvalidMessage');
                \Log::channel("send-notification-job")->info($e->errors());
            } catch (Exception $ex) {
                \Log::channel("send-notification-job")->info('SendNotificationJob::InvalidMessage');
                \Log::channel("send-notification-job")->info($ex);
            }
        }
        \Log::channel("send-notification-job")->info("SendNotificationJob::END");
    }

    /**
     * This will grab all device token of given userid.
     */

    public function getUserDeviceToken($userId)
    {
        $userData = User::where('user_id', $userId)->where('device_token', '<>', '')->whereNotNull('device_token')->whereIn('device_type', [UserToken::DEVICE_TYPE_ANDROID, UserToken::DEVICE_TYPE_IOS])->get()->pluck('device_token')->toArray();
        return $userData;
    }
}
