<?php

use App\Http\Controllers\API\RegisterController;

// V1 API starts here
use App\Http\Controllers\API\V1\CommonController;
use App\Http\Controllers\API\V1\ServiceProviderController;
use App\Http\Controllers\API\V1\UserController;
// V1 API Ends here

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function () {
    Route::post('user-home', [CommonController::class, 'userHome']); //- userHome
    Route::post('payment-response', [CommonController::class, 'paymentResponse']); //- userHome
    Route::get('get-inventory', [CommonController::class, 'getInventory']); //- getAboutUs
    Route::get('get-about-us', [CommonController::class, 'getAboutUs']); //- getAboutUs
    Route::get('get-configuration', [CommonController::class, 'getConfiguration']); //- getSliders
    Route::get('get-sliders', [CommonController::class, 'getSliders']); //- getSliders
    Route::get('get-time-slots', [CommonController::class, 'getTimeSlots']); //- getTimeSlots
    Route::get('get-service-brands', [CommonController::class, 'getServiceBrands']); //- getServiceBrands
    Route::get('get-services-categories', [CommonController::class, 'getServicesCategory']); //- getServicesCategory
    Route::get('fuel-types', [CommonController::class, 'getFuelType']); //- getFuelType
    Route::get('order-status-list', [CommonController::class, 'getorderStatusList']); //- getOrderStatusList
    Route::get('get-zones', [CommonController::class, 'getZones']); //- getZones
    Route::get('get-days', [CommonController::class, 'getDays']); //- getDays
    Route::get('city-list', [CommonController::class, 'getCityList']); //- getCityList
    Route::post('city-area-list', [CommonController::class, 'getCityAreaList']); //- getCityAreaList
    Route::get('brands', [CommonController::class, 'getBrand']); //- getBrand
    Route::post('brand-models', [CommonController::class, 'getBrandModel']); //- getBrandModel
    Route::get('get-banners', [CommonController::class, 'getBanners']); //- getBanners
    Route::get('get-rating-question', [CommonController::class, 'getRatingQuestions']); //- getRatingQuestions
    Route::get('get-faq', [CommonController::class, 'getFaq']); //- getFaq
    Route::post('analytics-logs', [CommonController::class, 'analyticsLog']); //- getFaq

    Route::middleware('auth:api')->group(function () {
        Route::post('check-user-slot', [CommonController::class, 'checkUserSlot']); //- update order status
        Route::post('update-order-status', [CommonController::class, 'updateOrderStatus']); //- update order status
        Route::post('get-service-by-id', [CommonController::class, 'getServiceById']); //- getOrderById        
        Route::post('order-status-tracking', [CommonController::class, 'orderStatusTracking']); //- orderStatusTracking
        Route::post('get-order-by-id', [CommonController::class, 'getOrderById']); //- getOrderById
        Route::post('generate-qr-code', [CommonController::class, 'generateQrCode']); //- generate Qr code for access
        Route::post('verify-qr-code', [CommonController::class, 'verifyQrCode']); //- generate Qr code for access
        Route::post('update-qr-code-status', [CommonController::class, 'updateQrCodeStatus']); //- Update Qr code for status
        Route::post('payment-intent', [CommonController::class, 'paymentIntent']); //- Payment Intialization
        Route::post('payment-cancel', [CommonController::class, 'paymentCancel'])->name('paymentCancel'); //- Payment Cancel 
        Route::post('payment-success', [CommonController::class, 'paymentSuccess'])->name('paymentSuccess'); //- Payment Success
        Route::post('submit-review', [CommonController::class, 'submitReview']); //- Payment Success


        Route::post('add-rating-reviews', [CommonController::class, 'addRatingReviews'])->name('addRatingReviews'); //- Add rating reviews
        Route::post('get-rating-reviews', [CommonController::class, 'getRatingReviews'])->name('getRatingReviews'); //- Get rating reviews

    });

    Route::post('service-provider/login', [ServiceProviderController::class, 'login']); // -Login
    Route::post('service-provider/technician-otp-verify', [ServiceProviderController::class, 'technicianOtpVerify']); // -technicianOtpVerify
    Route::post('service-provider/service-provider-otp-verify', [ServiceProviderController::class, 'serviceProviderOtpVerify']); // -serviceProviderOtpVerify
    Route::post('service-provider/forgot-password', [ServiceProviderController::class, 'forgotPassword']); // -forgotPassword
    Route::post('service-provider/forgot-password/verify-otp', [ServiceProviderController::class, 'forgotPasswordVerifyOtp']); // -forgotPasswordVerifyOtp
    Route::post('service-provider/reset-password', [ServiceProviderController::class, 'resetPassword']); // -forgotPasswordVerifyOtp

    Route::group(['prefix' => 'user'], function () {
        Route::post('send-otp', [UserController::class, 'SendOtp']); // -SendOtp
        Route::post('social-login', [UserController::class, 'socialLogin']); // -SendOtp
        Route::post('verify-otp', [UserController::class, 'VerifyOtp']); // -VerifyOtp
    });

    Route::middleware('auth:api')->group(function () {
        Route::post('logout', [RegisterController::class, 'logout']); // - Logout

        Route::group(['prefix' => 'technician'], function () {
            Route::post('get-order-by-date', [ServiceProviderController::class, 'getOrderByDate']); // -getOrderByDate
        });

        Route::group(['prefix' => 'service-provider'], function () {
            Route::post('reject-order', [ServiceProviderController::class, 'rejectOrder']); // -rejectOrder
            Route::post('assing-technician', [ServiceProviderController::class, 'assignTechnician']); // -assignTechnician
            Route::post('accept-order', [ServiceProviderController::class, 'acceptOrder']); // -acceptOrder
            Route::post('order-list', [ServiceProviderController::class, 'orderList']); // -orderList
            Route::post('service-provider-home', [ServiceProviderController::class, 'serviceProviderHome']); // -getNotification
            Route::post('get-notification', [ServiceProviderController::class, 'getNotification']); // -getNotification
            Route::post('logout', [ServiceProviderController::class, 'addTechinician']); // -addTechinician
            Route::post('add-technician', [ServiceProviderController::class, 'addTechinician']); // -addTechinician
            Route::post('get-technician-list', [ServiceProviderController::class, 'getTechnicianList']); // -getTechnicianList
            Route::post('set-up-service-provider-details', [ServiceProviderController::class, 'setUpServiceProviderDetails']); // -setUpServiceProviderDetails
            Route::post('service-provider-profile-update', [ServiceProviderController::class, 'serviceProviderProfileUpdate']); // -setUpServiceProviderDetails
            Route::post('verify-changed-mobile-number', [ServiceProviderController::class, 'verifyChangedMobileNumber']); // -verifyChangedMobileNumber
            Route::post('get-profile', [ServiceProviderController::class, 'getProfile']); // -getProfile
            Route::post('delete-technician', [ServiceProviderController::class, 'deleteTechnician']); // -getProfile
        });

        Route::group(['prefix' => 'user'], function () {
            Route::post('get-notification', [UserController::class, 'getNotification']); // -getNotification
            Route::post('get-car', [UserController::class, 'getCar']); // -getCar
            Route::post('remove-car', [UserController::class, 'removeCar']); // -getCar
            Route::post('get-my-cars', [UserController::class, 'getMyCars']); // -getMyCars
            Route::post('add-vehicle-information', [UserController::class, 'addVehicleInformation']); // -addVehicleInformation
            Route::post('update-profile', [UserController::class, 'updateProfile']); // -updateProfile
            Route::post('get-profile', [UserController::class, 'getProfile']); // -getProfile
            Route::post('add-order', [UserController::class, 'addOrder']); // -addOrder
            // Route::post('add-order', [UserController::class, 'addOrder']); // -addOrderV2
            Route::post('apply-promo', [UserController::class, 'applyPromo']); // -applyPromo
            Route::post('my-order-list', [UserController::class, 'myOrderList']); // -orderList
            Route::post('add-address', [UserController::class, 'addAddress']); // -addAddress
            Route::post('car-set-default', [UserController::class, 'carSetDefault']); // -carSetDefault
            Route::post('remove-address', [UserController::class, 'removeAddress']); // -removeAddress
            Route::post('get-my-address', [UserController::class, 'getMyAddress']); // -getMyAddress
            Route::post('get-address-by-id', [UserController::class, 'getAddressById']); // -getAddressById
            Route::post('address-set-default', [UserController::class, 'addressSetDefault']); // -addressSetDefault

        });
    });
});
