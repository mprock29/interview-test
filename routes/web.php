<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', [App\Http\Controllers\FrontUsersController::class, 'frontUser'])->name('front.user');
Route::get('/register', [App\Http\Controllers\FrontUsersController::class, 'frontUserRegister'])->name('front.user.register');
Route::get('/login', [App\Http\Controllers\FrontUsersController::class, 'frontUserLogin'])->name('front.login');
Route::get('/dashboard', [App\Http\Controllers\FrontUsersController::class, 'dashboard'])->name('front.dashboard')->middleware('auth');
Route::post('/login', [App\Http\Controllers\FrontUsersController::class, 'frontUserLoginSubmit'])->name('front.user.login');
Route::post('/submit', [App\Http\Controllers\FrontUsersController::class, 'frontUserRegisterSubmit'])->name('front.user.register.submit');
Route::prefix('admin')->group(function () {


Route::get('/', function () {
    return redirect('login');
});

Route::get('/restricted-url', [App\Http\Controllers\HomeController::class, 'restrictedUrl'])->name('user.restrictedUrl');

Route::any('/user/edit', [App\Http\Controllers\HomeController::class, 'edit'])->name('user.edit');
Route::get('/user/change/password', [App\Http\Controllers\ChangePasswordController::class, 'index'])->name('change');
Route::post('/user/change/password', [App\Http\Controllers\ChangePasswordController::class, 'store'])->name('change.password');

Auth::routes(['register' => false]);

Route::middleware(['common-settings', 'userRoles'])->group(function () {

    Route::any('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    // Route::get('/dashboard/{type}', [App\Http\Controllers\HomeController::class, 'getData'])->name('dashboard.getData');
    // Route::get('/dashboardorderlist', [App\Http\Controllers\HomeController::class, 'orderList'])->name('dashboard.orderList');

   

   
   
});
});