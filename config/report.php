<?php

return [
    'chunk_size' => 5000,
    'report_folder' => public_path().'/uploads/report_files',
];