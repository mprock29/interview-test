<?php

return [
    'address' => env('SOCKET_HOST', 'yhonk.php.dev.drcsystems.ooo'),
    'port' => env('SOCKET_PORT', '5005'),
    'protocol' => SOL_TCP
];
